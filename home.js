const months = ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
const monthdays = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
const days = ["", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
const daysMin = ["", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
const currentpage = { page: 'main' }
let mainstore
let resourcesParent = 0
let resourcesBreadCrumb = []


const thisDate = new Date()
const thisYear = thisDate.getFullYear()
const thisMonth = thisDate.getMonth()
const thisDay = thisDate.getDate()
let activeMonth = thisMonth
let activeYear = thisYear

function constructCalendar(y, m){
  document.getElementById('content').innerHTML = "<br /><h3>Calendar</h3><center><input type='button' value='<' onclick='changeMonth(false)' /><span style='width: 150px; display: inline-block;'>" + months[parseInt(m)+1] + " " + y + "</span><input type='button' value='>' onclick='changeMonth(true)' /></center><br />"
  drawCalendar(y, m)
  /*document.getElementById('content').innerHTML += `<br /><center>All events in ${months[m+1]} ${y}</center><br />`
  let aDay = thisDay + 1
  let i
  let leap = 0
  if(m==1){
    if(y % 4 == 0){
      leap = 1
      if(y % 100 == 0){
        leap = 0
        if(y % 400 == 0){
          leap = 1
        }
      }
    }
  }
  const iterations = leap ? monthdays[parseInt(m)+1] + 1 : monthdays[parseInt(m)+1]
  for(i=0; i < iterations; i++){
    const newLine = document.createElement('div')
    newLine.style.display = 'block'
    if(m == thisMonth && i + 1 == thisDay) { newLine.style.fontWeight = 'bold' }
    const getDayNameDate = new Date(y, m, i+1)
    newLine.innerHTML = (m + 1) + '/' + (i + 1) + ": " + days[getDayNameDate.getDay()+1]
    document.getElementById('content').appendChild(newLine)
  }
  //drawCalendar(y, m);
  */
}

function drawCalendar(y, m){
  if(window.innerWidth > 750){
    let i;
    const dateRef = new Date(y, m, 1);
    var dayRef = dateRef.getDay();
    let leap = 0;
    if(m==1){
      if(y % 4 == 0){
        leap = 1
        if(y % 100 == 0){
          leap = 0
          if(y % 400 == 0){
            leap = 1
          }
        }
      }
    }
    const numDays = leap && m == 1 ? monthdays[parseInt(m)+1] + 2 : monthdays[parseInt(m)+1] + 1
    if(document.getElementById('calendar')) document.getElementById('calendercontainer').removeChild(document.getElementById('calendar'))
    var cal=document.createElement('div'); cal.id = 'calendar'
    const w = `${99.6/7}%`
    cal.style.display = 'grid'; cal.style.gridTemplateColumns = `${w} ${w} ${w} ${w} ${w} ${w} ${w}`
    cal.style.backgroundColor = '#000'; cal.style.gridGap = '1px'
    cal.style.gridTemplateRows = 'auto auto auto auto auto'; cal.style.border = '1px black solid'
    const numRows = ((numDays-1) + dayRef) > 35 ? 8 : 7
    for(i=1; i<numRows; i++){ //build rows
      let ii
      for(ii=1; ii<8; ii++){ //build columns
        const newCell = document.createElement('div')
        newCell.style.backgroundColor = '#0F0FFF'; newCell.style.gridRow = i; newCell.style.gridColumn = ii
        if(i == 1){ newCell.appendChild(document.createTextNode(days[ii])); newCell.style.backgroundColor = '#d4af37' }
        else{ newCell.id = "day-" + (ii + ((i-2)*7)); newCell.style.height = '100px' }
        if(m == thisMonth && (ii + ((i-2)*7)) == thisDay + dayRef && y == thisYear) newCell.style.backgroundColor = '#3F3FFF'
        cal.appendChild(newCell)
      }
    }
    cal.style.visibility = 'hidden'
    document.getElementById('content').appendChild(cal)
    for(i=1; i<((numRows-2)*7)+1; i++){
      if(i > dayRef && i < dayRef+numDays){
        document.getElementById('day-' + i).appendChild(document.createTextNode(i-dayRef))
      } else document.getElementById('day-' + i).style.backgroundColor = '#0F0F90'
    }
    const cover = document.createElement('div'); cover.id = 'calcover'
    const calRect = cal.getBoundingClientRect()
    cover.style.position = 'absolute'; cover.style.backgroundColor = 'rgba(0, 0, 0, .9)'
    cover.style.left = calRect.left; cover.style.top = calRect.top + window.scrollY
    cover.style.width = calRect.width; cover.style.height = calRect.height
    cover.append('loading...')
    document.getElementById('content').appendChild(cover)
  }
  const ajax = new XMLHttpRequest()
  const req = new FormData()
  req.append('req', 'loadcal'); req.append('year', y); req.append('month', m+1)
  ajax.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      const res = JSON.parse(this.responseText)
      console.log(this.responseText)
      if(!res) return alert('Internal server error. Please try again.')
      if(res.events){ res.events.forEach((c) => {
        if(window.innerWidth > 750){
          const ev = document.createElement('a')
          ev.innerHTML = `<br />${c.title}`
          document.getElementById('day-' + (parseInt(c.day) + dayRef)).appendChild(ev)
          ev.onclick = function(){
            const desc = document.createElement('div'); desc.id = `caldesc-${c.day}`
            const parent = event.target.getBoundingClientRect()
            desc.innerText = c.desc.length ? c.desc : 'No description available.'
            desc.style.position = 'absolute'
            desc.style.top = parent.top + 20 + window.scrollY; desc.style.left = parent.left
            desc.style.backgroundColor = '#404040'; desc.style.padding = '5px'
            desc.style.borderRadius = '5px'; desc.style.border = '2px black solid'
            document.getElementById('content').appendChild(desc)
            const clear = function(){
              document.getElementById('content').removeChild(desc)
              document.removeEventListener('mousedown', clear)
            }
            document.addEventListener('mousedown', clear)
          }
        }else{
          const newevl = document.createElement('span'); newevl.className = 'eventanchorl'
          const newevr = document.createElement('a'); newevr.className = 'eventanchorr'
          const eventdesc = document.createElement('div'); eventdesc.className = 'eventdesc'
          const d = new Date(c.year, parseInt(c.month)-1, parseInt(c.day))
          let dayOut = `${c.day}`
          if(dayOut[dayOut.length - 1] == 0 || (dayOut > 10 && dayOut < 20)) dayOut += 'th'
          else if(dayOut[dayOut.length-1] == 1) dayOut += 'st'
          else if(dayOut[dayOut.length-1] == 2) dayOut += 'nd'
          else if(dayOut[dayOut.length-1] == 3) dayOut += 'rd'
          else dayOut += 'th'
          let hour = c.hour
          if(hour > 12) hour -= 12; if(hour == 0) hour = 12
          newevl.innerHTML = `${daysMin[parseInt(d.getDay())+1]}, ${c.month}/${c.day}@${hour}:${c.minute < 10 ? `0${c.minute}` : c.minute}${c.hour < 12 ? 'am' : 'pm'} -`
          newevr.innerHTML = ` ${c.title}`
          eventdesc.innerHTML = c.desc ? c.desc : 'No description available.'; eventdesc.style.display = 'none'
          document.getElementById('content').appendChild(newevl)
          document.getElementById('content').appendChild(newevr)
          document.getElementById('content').appendChild(eventdesc)
          newevr.onclick = function() {
            if(eventdesc.style.display == 'none'){
              const evs = document.getElementById('content').getElementsByClassName('eventdesc')
              let i = 0
              for(i; i<evs.length; ++i){
                evs[i].style.display = 'none'
              }
              eventdesc.style.display = 'block'
            } else eventdesc.style.display = 'none'
          }
        }
      //document.getElementById('day-' + (parseInt(c.day) + dayRef)).innerHTML += `<br />${c.title}`
      })}
      if(window.innerWidth > 750) document.getElementById('calcover').style.display = 'none'
    }
  }
  ajax.open('POST', 'index.php')
  ajax.send(req)
  if(window.innerWidth>750) cal.style.visibility = 'visible'
}

function changeMonth(dir){
  if(dir){
    if(activeMonth < 11) activeMonth++
    else{ activeMonth = 0; activeYear++ }
  }else{
    if(activeMonth > 0) activeMonth--
    else{ activeMonth = 11; activeYear-- }
  }
  constructCalendar(activeYear, activeMonth)
}

document.addEventListener('DOMContentLoaded', ()=>{
  mainstore = document.getElementById('content').inneHTML
  document.getElementById('nav').addEventListener('mouseover', function(){ navHover(event, true) })
  document.getElementById('nav').addEventListener('mouseout', function(){ navHover(event, false) })
  /*if(document.getElementById('popupbg')){
    document.getElementById('popupbg').onclick = function(){ this.style.display = 'none' }
  }*/
  const req = new FormData(); req.append('req', 'getannouncement')
  ajaxRequest(req)
  const url = new URL(window.location)
  if(url.searchParams.get('postid')){
    const req = new FormData()
    req.append('req', 'blogpost'); req.append('id', url.searchParams.get('postid'))
    ajaxRequest(req)
  }else if(url.searchParams.get('resourceid')){
    const req = new FormData()
    req.append('req', 'resource'); req.append('id', url.searchParams.get('resourceid'))
    ajaxRequest(req)
  }else{
    let req = new FormData()
    const d = new Date()
    const mo = d.getMonth() > 9 ? d.getMonth()+1 : `0${d.getMonth()+1}`
    const da = d.getDate() > 9 ? d.getDate() : `0${d.getDate()}`
    const outDate = `${d.getFullYear()}${mo}${da}0000`
    req.append('req', 'getcal'); req.append('date', outDate)
    req.append('year', d.getFullYear()); req.append('month', parseInt(d.getMonth()) + 1)
    ajaxRequest(req)
    req = new FormData()
    req.append('req', 'getblog'); req.append('offset', 0); req.append('lim', 3)
    ajaxRequest(req)
    /*req = new FormData()
    req.append('req', 'getannouncement')
    ajaxRequest(req)*/
  }
  /*if(window.innerWidth < 750){
    document.getElementsByClassName('tithely-give-btn')[0].style.position = 'static'
    document.getElementsByClassName('tithely-give-btn')[0].style.padding = '3px 5px'
    document.getElementsByClassName('tithely-give-btn')[0].style.fontWeight = 'normal'
  }*/
})

function loadMain(){
  if(currentpage.page != 'main'){
    document.getElementById('nav').childNodes[1].style.backgroundColor = '#0F0FFF'
    document.getElementById('nav').childNodes[3].style.backgroundColor = '#800020'
    document.getElementById('nav').childNodes[5].style.backgroundColor = '#800020'
    document.getElementById('nav').childNodes[7].style.backgroundColor = '#800020'
    document.getElementById('content').innerHTML = mainstore
    currentpage.page = 'main'
    let req = new FormData()
    const d = new Date()
    const outDate = `${d.getFullYear()}${(parseInt(d.getMonth()) + 1) < 10 ? `0${parseInt(d.getMonth()) + 1}` : parseInt(d.getMonth()) + 1}${parseInt(d.getDate()) < 10 ? `0${parseInt(d.getDate())}` : parseInt(d.getDate)}0000`
    console.log(`OUT DATE: ${outDate}`)
    req.append('req', 'getcal'); req.append('date', outDate)
    req.append('year', d.getFullYear()); req.append('month', parseInt(d.getMonth()) + 1)
    ajaxRequest(req)
    req = new FormData()
    req.append('req', 'getblog'); req.append('offset', 0); req.append('lim', 3)
    ajaxRequest(req)
  }
}

function loadCal(){
  if(currentpage.page != 'cal'){
    document.getElementById('nav').childNodes[1].style.backgroundColor = '#800020'
    document.getElementById('nav').childNodes[3].style.backgroundColor = '#0F0FFF'
    document.getElementById('nav').childNodes[5].style.backgroundColor = '#800020'
    document.getElementById('nav').childNodes[7].style.backgroundColor = '#800020'
    if(currentpage.page == 'main') mainstore = document.getElementById('content').innerHTML
    constructCalendar(thisYear, thisMonth)
    currentpage.page = 'cal'
  }
}

function loadBlog(c){
  if(currentpage.page == 'main') mainstore = document.getElementById('content').innerHTML
  currentpage.page = 'blog'
  document.getElementById('nav').childNodes[1].style.backgroundColor = '#800020'
  document.getElementById('nav').childNodes[3].style.backgroundColor = '#800020'
  document.getElementById('nav').childNodes[5].style.backgroundColor = '#0F0FFF'
  document.getElementById('nav').childNodes[7].style.backgroundColor = '#800020'
  while(document.getElementById('content').lastChild) document.getElementById('content').removeChild(document.getElementById('content').lastChild)
  const blog = document.createElement('div'); blog.id = 'blog'
  document.getElementById('content').appendChild(blog)
  if(c){
    const yr = c.date.substring(0,4), mn = c.date.substring(4,6), dy = c.date.substring(6,8)
    const hr = c.date.substring(8,10), mi = c.date.substring(10,12)
    const newPost = document.createElement('div'); newPost.className = 'fullblogpost'
    const blogContent = document.createElement('div'); blogContent.innerHTML = c.content; blogContent.className = 'blogcontent'
    blogContent.style.backgroundColor = '#0F0FC0'
    newPost.innerHTML += `<h4>${c.title}</h4><small>Posted by ${c.author} at ${hr > 12 ? parseInt(hr) - 12 : parseInt(hr)}:${mi}${hr > 12 ? 'pm' : 'am'} on ${mn}/${dy}/${yr}</small><br />`
    newPost.appendChild(blogContent)
    const url = new URL(window.location)
    const link = `${url.hostname}${url.pathname}?postid=${c.id}`
    newPost.innerHTML += `<br><small>Link to this post: <a href='${link}'>${link}</a></small>`
    const viewAll = document.createElement('a'); viewAll.innerHTML = '<small>< View all news posts</small>'
    document.getElementById('blog').prepend(viewAll)
    document.getElementById('blog').appendChild(newPost)
    viewAll.onclick = () => loadBlog()
  }else{
    const form = new FormData()
    form.append('req', 'getfullblog'); form.append('offset', 0); form.append('lim', 10)
    ajaxRequest(form)
  }
}

function loadResources(jObj){
  if(currentpage.page != 'res'){
    document.getElementById('nav').childNodes[1].style.backgroundColor = '#800020'
    document.getElementById('nav').childNodes[3].style.backgroundColor = '#800020'
    document.getElementById('nav').childNodes[5].style.backgroundColor = '#800020'
    document.getElementById('nav').childNodes[7].style.backgroundColor = '#0F0FFF'
    if(currentpage.page == 'main') mainstore = document.getElementById('content').innerHTML
    currentpage.page = 'res'
  }
  if(jObj){
    renderResources(jObj)
  }else{
    const form = new FormData()
    form.append('req', 'getresources')
    form.append('parent', 0)
    ajaxRequest(form)
  }
}
function renderResources(jObj){
  while(document.getElementById('content').lastChild){
    document.getElementById('content').removeChild(document.getElementById('content').lastChild)
  }
  const rd = document.createElement('div'); rd.id = 'resources'
  const bc = document.createElement('div'); bc.id = 'resourcesbreadcrumb'
  let home = document.createElement('a');
  if(resourcesParent == 0){ home = document.createElement('span'); home.style.fontWeight = 'bold' }
  home.innerText = 'Resources Home'
  bc.insertAdjacentElement('afterbegin', home)
  if(resourcesParent != 0) home.onclick = ()=>{
    const req = new FormData(); req.append('req', 'getresources'); req.append('parent', 0)
    ajaxRequest(req);
    resourcesBreadCrumb = []
  }
  let i
  for(i=0; i<resourcesBreadCrumb.length; ++i){
    if(resourcesBreadCrumb[i]){
      if(i == resourcesParent){
        bc.insertAdjacentHTML('beforeend', ` > <b>${resourcesBreadCrumb[i]}</b>`)
        let ii
        for(ii=1; i+ii<resourcesBreadCrumb.length; ++ii){
          resourcesBreadCrumb[i+ii] = null
        }
      }else{
        const a = document.createElement('a')
        a.innerText = ` > ${resourcesBreadCrumb[i]}`
        bc.insertAdjacentElement('beforeend', a)
        a.onclick = (function(i){
          return function(){
            const req = new FormData(); req.append('req', 'getresources'); req.append('parent', i)
            ajaxRequest(req)
          }
        })(i)
      }
    }
  }
  rd.insertAdjacentHTML('beforeend', '<h3>Resources</h3>')
  rd.insertAdjacentElement('beforeend', bc)
  let rt = document.createElement('table')
  let rr = document.createElement('tr')
  let rc = document.createElement('td')
  rt.id = 'resourcestable'
  if(jObj.resources && jObj.resources.length > 0){
    jObj.resources.forEach((c)=>{
      rr = document.createElement('tr')
      rc = document.createElement('td')
      rc.innerText = `${c.title} `
      let rb = document.createElement('a')
      if(c.category) rb.innerText = 'More >'; else rb.innerText = 'View >'
      rc.insertAdjacentElement('beforeend', rb)
      if(!c.category){
        let date = `${c.date}`
        let datetime = new Date(date.substring(0,4), parseInt(date.substring(4,6))-1, parseInt(date.substring(6,8)), date.substring(8,10), date.substring(10,12), date.substring(12,14))
        rc.insertAdjacentHTML('beforeend', `<br /><small>Author: ${c.authorname} | Posted on: ${days[parseInt(datetime.getDay())+1]}, ${months[parseInt(datetime.getMonth())+1]} ${parseInt(datetime.getDate())}, ${datetime.getFullYear()} at ${datetime.toLocaleTimeString()}`)
      }
      const rcon = document.createElement('div')
      rcon.className = 'resourcecontent'; rcon.innerHTML = c.content
      rcon.style.display = 'none'
      rc.insertAdjacentElement('beforeend', rcon)
      rr.onclick = ()=>{
        if(c.category){
          resourcesBreadCrumb[c.id] = c.title
          const req = new FormData()
          req.append('req', 'getresources'); req.append('parent', c.id)
          ajaxRequest(req)
        }else{
          resourcesBreadCrumb[c.id] = c.title
          resourcesParent = c.id
          renderResources(c)
        }
      }
      rr.appendChild(rc)
      rt.insertAdjacentElement('beforeend', rr)
    })
  }else if(jObj.date){
    let date = `${jObj.date}`
    let datetime = new Date(date.substring(0,4), parseInt(date.substring(4,6))-1, parseInt(date.substring(6,8)), date.substring(8,10), date.substring(10,12), date.substring(12,14))
    const newPost = document.createElement('div'); newPost.className = 'fullblogpost'
    const postContent = document.createElement('div'); postContent.className = 'blogcontent'
    const url = new URL(window.location)
    const link = `${url.hostname}${url.pathname}?resourceid=${jObj.id}`
    newPost.insertAdjacentHTML('beforeend', `<b>${jObj.title}</b><br /><small>Author: ${jObj.authorname} | Posted on: ${days[parseInt(datetime.getDay())+1]}, ${months[parseInt(datetime.getMonth())+1]} ${parseInt(datetime.getDate())}, ${datetime.getFullYear()} at ${datetime.toLocaleTimeString()}<br /><a href='${link}'>${link}</a></small><br /><br />`)
    postContent.insertAdjacentHTML('beforeend', jObj.content)
    newPost.insertAdjacentElement('beforeend', postContent)
    rc.insertAdjacentElement('beforeend', newPost)
    rr.appendChild(rc)
    rt.insertAdjacentElement('beforeend', rr)
  }else{
    rc.innerText = 'Nothing to show.'
    rr.appendChild(rc)
    rt.appendChild(rr)
  }
  rd.insertAdjacentElement('beforeend', rt)
  document.getElementById('content').insertAdjacentElement('afterbegin', rd)
}


function navHover(ev, hov){
  const nodes = document.getElementById('nav').childNodes
  if(ev.target == nodes[1] && currentpage.page != 'main'){
    if(hov) document.getElementById('nav').childNodes[1].style.backgroundColor = '#E4BF47' //#303030 - old
    else document.getElementById('nav').childNodes[1].style.backgroundColor = '#800020'//'#d4af37' //#202020 - old
  }
  else if(ev.target == nodes[3] && currentpage.page != 'cal'){
    if(hov) document.getElementById('nav').childNodes[3].style.backgroundColor = '#E4BF47'
    else document.getElementById('nav').childNodes[3].style.backgroundColor = '#800020'//'#d4af37'
  }
  else if(ev.target == nodes[5] && currentpage.page != 'blog'){
    if(hov) document.getElementById('nav').childNodes[5].style.backgroundColor = '#E4BF47'
    else document.getElementById('nav').childNodes[5].style.backgroundColor = '#800020'//'#d4af37'
  }
  else if(ev.target == nodes[7] && currentpage.page != 'res'){
    if(hov) document.getElementById('nav').childNodes[7].style.backgroundColor = '#E4BF47'
    else document.getElementById('nav').childNodes[7].style.backgroundColor = '#800020'//'#d4af37'
  }
}

function requestResource(id){
  const form = new FormData()
  form.append('req', 'resource')
  form.append('id', id)
  ajaxRequest(form)
}

function ajaxRequest(req){
  const ajax = new XMLHttpRequest()
  ajax.onreadystatechange = function(){
    if(this.status == 200 && this.readyState == 4){
      console.log(this.responseText)
      const res = JSON.parse(this.responseText)
      if(!res.type) return alert('Internal server error. Please try again.')
      if(res.type == 'getcal'){
        if(res.events) while(document.getElementById('events').lastChild){
          document.getElementById('events').removeChild(document.getElementById('events').lastChild)
        }
        /*document.getElementById('events').innerHTML = '(Click on an event title for more info) -'
        const calanchor = document.createElement('a'); calanchor.innerText = ' Click here to view the full calendar'
        calanchor.href = '#'
        document.getElementById('events').appendChild(calanchor)
        document.getElementById('events').append(document.createElement('br'))*/
        if(res.events) res.events.forEach((c)=>{
          const newevl = document.createElement('span'); newevl.className = 'eventanchorl'
          const newevr = document.createElement('a'); newevr.className = 'eventanchorr'
          const eventdesc = document.createElement('div'); eventdesc.className = 'eventdesc'
          const d = new Date(c.year, parseInt(c.month)-1, parseInt(c.day))
          let dayOut = `${c.day}`
          if(dayOut[dayOut.length - 1] == 0 || (dayOut > 10 && dayOut < 20)) dayOut += 'th'
          else if(dayOut[dayOut.length-1] == 1) dayOut += 'st'
          else if(dayOut[dayOut.length-1] == 2) dayOut += 'nd'
          else if(dayOut[dayOut.length-1] == 3) dayOut += 'rd'
          else dayOut += 'th'
          let hour = c.hour
          if(hour > 12) hour -= 12; if(hour == 0) hour = 12
          newevl.innerHTML = `${days[parseInt(d.getDay())+1]}, ${months[c.month]} ${dayOut} @ ${hour}:${c.minute < 10 ? `0${c.minute}` : c.minute}${c.hour < 12 ? 'am' : 'pm'} -`
          newevr.innerHTML = ` ${c.title}`
          eventdesc.innerHTML = c.desc ? c.desc : 'No description available.'; eventdesc.style.display = 'none'
          document.getElementById('events').appendChild(newevl)
          document.getElementById('events').appendChild(newevr)
          document.getElementById('events').appendChild(eventdesc)
          newevr.onclick = function() {
            if(eventdesc.style.display == 'none'){
              const evs = document.getElementById('events').getElementsByClassName('eventdesc')
              let i = 0
              for(i; i<evs.length; ++i){
                evs[i].style.display = 'none'
              }
              eventdesc.style.display = 'block'
            } else eventdesc.style.display = 'none'
          }
        })
      }
      if(res.type == 'getblog'){
        //$res -> posts[$rowcount] = array('id' => $post['id'], 'authorid' => $post['author'], 'author' => $authorname, 'title' => $post['title'], 'content' => $post['content'], 'date' => $post['date']);
        if(res.posts) if(document.getElementById('blog').lastChild){
          while(document.getElementById('blog').lastChild){
            document.getElementById('blog').removeChild(document.getElementById('blog').lastChild)
          }
        }
        if(res.posts) res.posts.forEach((c)=>{
          const yr = c.date.substring(0,4), mn = c.date.substring(4,6), dy = c.date.substring(6,8)
          const hr = c.date.substring(8,10), mi = c.date.substring(10,12)
          const newPost = document.createElement('div'); newPost.className = 'blogpost'
          const blogContent = document.createElement('div'); blogContent.innerHTML = c.content; blogContent.className = 'blogcontent'
          let i;
          for(i=0; i < blogContent.getElementsByTagName('img').length; i++){
            blogContent.removeChild(blogContent.getElementsByTagName('img')[i])
          }
          if(blogContent.getElementsByClassName('preview').length > 0){
            blogContent.innerHTML = blogContent.getElementsByClassName('preview')[0].innerHTML
            const seemore = document.createElement('a'); seemore.innerText = 'Read full post'
            seemore.style.marginLeft = '5px'
            blogContent.appendChild(seemore)
          }else if(blogContent.innerHTML.length > 500){
            blogContent.innerHTML = blogContent.innerHTML.substring(0,500) + '...'
            const seemore = document.createElement('a'); seemore.innerText = 'Read full post'
            seemore.style.marginLeft = '5px'
            blogContent.appendChild(seemore)
          }
          blogContent.style.backgroundColor = '#0F0FC0'
          newPost.innerHTML += `<h4>${c.title}</h4><small>Posted by ${c.author} at ${hr > 12 ? parseInt(hr) - 12 : parseInt(hr)}:${mi}${hr > 12 ? 'pm' : 'am'} on ${mn}/${dy}/${yr}</small><br />`
          newPost.appendChild(blogContent)
          document.getElementById('blog').appendChild(newPost)
          newPost.onclick = () => loadBlog(c)
        })
        const viewAll = document.createElement('a')
        viewAll.innerText = 'View all news posts'
        document.getElementById('blog').appendChild(viewAll)
        viewAll.onclick = () => loadBlog()
      }
      if(res.type == 'getfullblog'){
        //$res -> posts[$rowcount] = array('id' => $post['id'], 'authorid' => $post['author'], 'author' => $authorname, 'title' => $post['title'], 'content' => $post['content'], 'date' => $post['date']);
        if(res.posts.length < 1) document.getElementById('blog').innerHTML = "No posts found :-("
        else{
          while(document.getElementById('blog').lastChild) document.getElementById('blog').removeChild(document.getElementById('blog').lastChild)
          let l = document.createElement('input'); let r = document.createElement('input')
          const numPages = Math.ceil(res.rows/10)
          const currentPage = (parseInt(res.offset) + 10) / 10
          l.type = 'button'; r.type = 'button'; l.value = '<'; r.value = '>'
          function browse(dir){
            const form = new FormData()
            form.append('req', 'getfullblog'); form.append('lim', 10); form.append('offset', dir ? parseInt(res.offset) + 10 : parseInt(res.offset) - 10)
            ajaxRequest(form)
          }
          if(currentPage == 1) l.disabled = 'true'
          if(currentPage == numPages) r.disabled = 'true'
          blog.insertAdjacentHTML('afterbegin', '<h3>News</h3>')
          let cent = document.createElement('center')
          cent.append(l, `Page ${currentPage} of ${numPages}`, r)
          document.getElementById('blog').insertAdjacentElement('beforeend', cent)
          l.onclick = ()=>browse(null); r.onclick = ()=>browse(true)
          res.posts.forEach((c)=>{
            const yr = c.date.substring(0,4), mn = c.date.substring(4,6), dy = c.date.substring(6,8)
            const hr = c.date.substring(8,10), mi = c.date.substring(10,12)
            const newPost = document.createElement('div'); newPost.className = 'blogpost'
            const blogContent = document.createElement('div'); blogContent.innerHTML = c.content; blogContent.className = 'blogcontent'
            let i;
            for(i=0; i < blogContent.getElementsByTagName('img').length; i++){
              blogContent.removeChild(blogContent.getElementsByTagName('img')[i])
            }
            if(blogContent.getElementsByClassName('preview').length > 0){
              blogContent.innerHTML = blogContent.getElementsByClassName('preview')[0].innerHTML
              const seemore = document.createElement('a'); seemore.innerText = 'Read full post'
              seemore.style.marginLeft = '5px'
              blogContent.appendChild(seemore)
            }else if(blogContent.innerHTML.length > 500){
              blogContent.innerHTML = blogContent.innerHTML.substring(0,500) + '...'
              const seemore = document.createElement('a'); seemore.innerText = 'Read full post'
              seemore.style.marginLeft = '5px'
              blogContent.appendChild(seemore)
            }
            blogContent.style.backgroundColor = '#0F0FC0'
            newPost.innerHTML += `<h4>${c.title}</h4><small>Posted by ${c.author} at ${hr > 12 ? parseInt(hr) - 12 : parseInt(hr)}:${mi}${hr > 12 ? 'pm' : 'am'} on ${mn}/${dy}/${yr}</small><br />`
            newPost.appendChild(blogContent)
            document.getElementById('blog').appendChild(newPost)
            newPost.onclick = () => loadBlog(c)
          })
          l = document.createElement('input'); r = document.createElement('input')
          l.type = 'button'; r.type = 'button'; l.value = '<'; r.value = '>'
          if(currentPage == 1) l.disabled = 'true'
          if(currentPage == numPages) r.disabled = 'true'
          cent = document.createElement('center')
          cent.append(l, `Page ${currentPage} of ${numPages}`, r)
          document.getElementById('blog').insertAdjacentElement('beforeend', cent)
          l.onclick = ()=>browse(null); r.onclick = ()=>browse(true)
        }
      }
      if(res.type == 'blogpost'){
        loadBlog(res)
      }
      if(res.type == 'message'){
        alert(res.message)
      }
      if(res.type == 'getannouncement'){
        if(res.antype != 'hidden'){
          if(res.antype == 'popup'){
            const popupbg = document.createElement('div'); popupbg.id = 'popupbg'
            const popup = document.createElement('div'); popup.id = 'popup'
            const popupcont = document.createElement('div'); popupcont.id = 'popupcont'
            const popuphold = document.createElement('div'); popuphold.id = 'popuphold'
            const close = document.createElement('a')
            close.style = 'position: absolute; top: 30; right: 30; text-decoration: none;'
            close.innerHTML = '<big>Click anywhere to close this announcement</big>'
            popupbg.style.color = 'white'; popupbg.style.fontFamily = 'Roboto-Light'
            popuphold.style.color = 'white'; popuphold.style.fontFamily = 'Roboto-Light'
            popuphold.innerHTML = res.ancontent
            popupcont.appendChild(popuphold); popup.appendChild(popupcont); popupbg.appendChild(popup)
            popupbg.appendChild(close)
            document.getElementById('content').insertAdjacentElement('beforebegin', popupbg)
            popupbg.style.display = 'table'
            popupbg.onclick = function(){ this.style.display = 'none' }
          }else{
            const an = document.createElement('div')
            an.innerHTML = res.ancontent
            if(currentpage.page == 'main') document.getElementById('servicetimes').insertAdjacentElement('afterend',an)
            else{
              const holder = document.createElement('div')
              holder.appendChild(an)
              mainstore = holder.innerHTML + mainstore
            }
          }
        }
      }
      if(res.type == 'getresources'){
        resourcesParent = res.parent
        renderResources(res)
      }
      if(res.type == 'resource'){
        resourcesParent = res.id
        loadResources(res)
      }
    }
  }
  ajax.open('POST', 'index.php')
  ajax.send(req)
}
