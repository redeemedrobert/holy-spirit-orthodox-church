<html>
<title>HSOC Calendar</title>
<script type='text/javascript'>
var d = new Date();
var thisYear = d.getFullYear();
var thisMonth = d.getMonth();
var thisDay = d.getDate();
var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
var monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
var dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
var activeMonth = thisMonth;
var activeYear = thisYear;

function constructCalendar(y, m)
{
  document.getElementById('calendercontainer').innerHTML = "<center><input type='button' value='<' onclick='changeMonth(false)' /> " + monthNames[m] + " " + y + " <input type='button' value='>' onclick='changeMonth(true)' /></center><br />";
  /*
  var aDay = thisDay + 1;
  var i;
  var leap = 0;
  if(m==1)
  {
    if(y % 4 == 0)
    {
      leap = 1;
      if(y % 100 == 0)
      {
        leap = 0;
        if(y % 400 == 0)
        {
          leap = 1;
        }
      }
    }
  }
  var iterations = leap ? monthDays[m] + 1 : monthDays[m];
  for(i=0; i < iterations; i++)
  {
    var newLine = document.createElement('div');
    newLine.style.display = 'block';
    if(m == thisMonth && i + 1 == thisDay) { newLine.style.fontWeight = 'bold'; }
    var getDayNameDate = new Date(y, m, i+1);
    newLine.innerHTML = (m + 1) + '/' + (i + 1) + ": " + dayNames[getDayNameDate.getDay()];
    document.getElementById('calendercontainer').appendChild(newLine);
  }*/
  drawCalendar(y, m);
}

function drawCalendar(y, m)
{
  var i;
  var dateRef = new Date(y, m, 1);
  var dayRef = dateRef.getDay();
  var leap = 0;
  if(m==1)
  {
    if(y % 4 == 0)
    {
      leap = 1;
      if(y % 100 == 0)
      {
        leap = 0;
        if(y % 400 == 0)
        {
          leap = 1;
        }
      }
    }
  }
  var numDays = leap && m == 1 ? monthDays[m] + 2 : monthDays[m] + 1;
  if(document.getElementById('calendar')){ document.getElementById('calendercontainer').removeChild(document.getElementById('calendar')); }
  var cal=document.createElement('div'); cal.id = 'calendar';
  cal.style.display = 'grid'; cal.style.gridTemplateColumns = 'auto auto auto auto auto auto';
  cal.style.backgroundColor = '#000'; cal.style.gridGap = '1px';
  cal.style.gridTemplateRows = 'auto auto auto auto auto'; cal.style.border = '1px black solid';
  var numRows = ((numDays-1) + dayRef) > 35 ? 8 : 7;
  for(i=1; i<numRows; i++) { //build rows
    var ii;
    for(ii=1; ii<8; ii++) { //build columns
      var newCell = document.createElement('div');
      newCell.style.backgroundColor = 'rgba(200, 200, 200, 1)'; newCell.style.gridRow = i; newCell.style.gridColumn = ii;
      if(i == 1) newCell.appendChild(document.createTextNode(dayNames[ii-1]));
      else{ newCell.id = "day-" + (ii + ((i-2)*7)); newCell.style.height = '100px'; }
      if(m == thisMonth && (ii + ((i-2)*7)) == thisDay + dayRef && y == thisYear) newCell.style.backgroundColor = 'rgba(115, 115, 255, 1)';
      cal.appendChild(newCell);
    }
  }
  cal.style.visibility = 'hidden';
  document.getElementById('calendercontainer').appendChild(cal);
  /*for(i=1; i<numDays; i++) //populate cells
  {
    document.getElementById('day-' + (i + dayRef)).appendChild(document.createTextNode(i));
  }*/;
  for(i=1; i<((numRows-2)*7)+1; i++)
  {
    if(i > dayRef && i < dayRef+numDays)
    {
      document.getElementById('day-' + i).appendChild(document.createTextNode(i-dayRef));
    }
    else document.getElementById('day-' + i).style.backgroundColor = '#959595';
  }
  cal.style.visibility = 'visible';
}

function changeMonth(dir)
{
  if(dir) {
    if(activeMonth < 11) activeMonth++;
    else { activeMonth = 0; activeYear++; }
  } else {
    if(activeMonth > 0) activeMonth--;
    else { activeMonth = 11; activeYear--; }
  }
  constructCalendar(activeYear, activeMonth);
}
document.addEventListener("DOMContentLoaded", function(){ constructCalendar(thisYear, thisMonth); });
</script>
<body>
  <div id='calendercontainer'>
  </div>
</body>
</html>
