class settingsUI{
  constructor(){
    this.elem = {
      settingsTable: document.createElement('table'),
      //admin password
      passRow: document.createElement('tr'),
      passTdL: document.createElement('td'), passTdR: document.createElement('td'),
      changePassButton: document.createElement('input'),
      //user management
      userRow: document.createElement('tr'),
      userTdL: document.createElement('td'), userTdR: document.createElement('td'),
      userButton: document.createElement('input'),
      //calendar management
      calRow: document.createElement('tr'),
      calTdL: document.createElement('td'), calTdR: document.createElement('td'),
      calButton: document.createElement('input'),
      //blog
      blogDiv: document.createElement('div'),
      blogTable: document.createElement('table'),
      blogRow: document.createElement('tr'),
      blogTdL: document.createElement('td'), blogTdR: document.createElement('td'),
      createPostBg: document.createElement('div'), createPostButton: document.createElement('input'),
      blogEditButton: document.createElement('input'), blogDelButton: document.createElement('input'),
      //popup
      popUpBg: document.createElement('div'),
      popUp: document.createElement('div'),
      //Admin password popup
      cont: document.createElement('div'),
      hold: document.createElement('div'),
      currentPass: document.createElement('input'),
      passField1: document.createElement('input'),
      passField2: document.createElement('input'),
      confirm: document.createElement('input'),
      cancel: document.createElement('input'),
      //User management popup
      //'users'('id' INTEGER PRIMARY KEY, 'uname' TEXT, 'name' TEXT, 'pword' TEXT, 'blogpost' INTEGER, 'calendar' INTEGER, 'admin' INTEGER)
      umTable: document.createElement('table'),
      umRow: document.createElement('tr'),
      umCell: document.createElement('td'),
      umEditButton: document.createElement('input'),
      umDelButton: document.createElement('input'),
      umAddButton: document.createElement('input'),
      umUName: document.createElement('input'),
      umPWord: document.createElement('input'),
      umB: document.createElement('input'),
      umC: document.createElement('input'),
      umA: document.createElement('input'),
      //User add/edit popup
      uaeInput: document.createElement('input'),
      //post edit
      imageContDisplay: true
    }

    this.resourcesParent = 0
    this.resourcesBreadCrumb = []
    //popups stuff
    this.elem.cont.id = 'popupcont'; this.elem.hold.id = 'popuphold'
  }
  makeElements(){
    this.elem.settingsTable.id = 'settingstable'

    //admin password
    if(config.id == 0){
      this.elem.changePassButton.type = 'button'; this.elem.changePassButton.value = 'Change'
      this.elem.changePassButton.onclick = () => { this.renderPopUp(this.makeAPassPopUp()) }
      this.elem.passTdL.innerText = 'Change admin password: '
      this.elem.passTdR.appendChild(this.elem.changePassButton)
      this.elem.passTdL.className = 'settingstdl'; this.elem.passTdR.className = 'settingstdr'
      this.elem.passRow.appendChild(this.elem.passTdL); this.elem.passRow.appendChild(this.elem.passTdR)
      this.elem.settingsTable.appendChild(this.elem.passRow)
    }

    //user management
    if(config.admin){
      this.elem.userButton.type = 'button'; this.elem.userButton.value = 'Manage'
      this.elem.userButton.addEventListener('click', function(){ ajaxRequest('req=getusers') })
      this.elem.userTdL.innerText = 'Manage users: '
      this.elem.userTdR.appendChild(this.elem.userButton)
      this.elem.userTdL.className = 'settingstdl'; this.elem.userTdR.className = 'settingstdr'
      this.elem.userRow.appendChild(this.elem.userTdL); this.elem.userRow.appendChild(this.elem.userTdR)
      this.elem.settingsTable.appendChild(this.elem.userRow)
    }

    //calendar management
    if(config.calendar || config.admin){
      let d = new Date()
      this.elem.calButton.type = 'button'; this.elem.calButton.value = 'Manage'
      this.elem.calButton.addEventListener('click', () => ajaxRequest(`req=getcal&month=${d.getMonth() + 1}&year=${d.getFullYear()}`))
      this.elem.calTdL.innerText = 'Manage calendar: '
      this.elem.calTdR.appendChild(this.elem.calButton)
      this.elem.calTdL.className = 'settingstdl'; this.elem.calTdR.className = 'settingstdr'
      this.elem.calRow.appendChild(this.elem.calTdL); this.elem.calRow.appendChild(this.elem.calTdR)
      this.elem.settingsTable.appendChild(this.elem.calRow)
    }

    //announcement management
    if(config.announce || config.admin){
      const newR = document.createElement('tr')
      const tdL = document.createElement('td'), tdR = document.createElement('td')
      tdL.innerText = 'Manage announcement: '
      tdL.className = 'settingstdl'; tdR.className = 'settingstdr'
      const anButton = document.createElement('input')
      anButton.type = 'button'; anButton.value = 'Manage'
      tdR.appendChild(anButton)
      newR.appendChild(tdL); newR.appendChild(tdR)
      this.elem.settingsTable.appendChild(newR)
      anButton.onclick = () => ajaxRequest('req=getannouncement')
    }
  }

  makeBlogElements(jObj){
    var blogTable = document.createElement('table')
    this.elem.blogDiv.id = 'blogdiv'
    blogTable.id = 'blogtable'
    this.elem.blogDiv.innerHTML = '<h3>News Management</h3>'
    this.elem.createPostBg.className = 'createpostbg'
    this.elem.createPostButton.id = 'createpostbutton'
    this.elem.createPostButton.type = 'button'; this.elem.createPostButton.value = 'New Post'
    this.elem.createPostButton.addEventListener('click', () => { this.renderPopUp(this.addBlog()); this.addBlogFunctions() })
    this.elem.createPostBg.appendChild(this.elem.createPostButton)
    this.elem.blogDiv.appendChild(this.elem.createPostBg)
    if(jObj.posts){jObj.posts.forEach(function(c){
      if(config.admin || c.authorid == config.id){
        let dateRef = c.date
        let date = new Date(dateRef.substring(0,4), dateRef.substring(4,6) - 1, dateRef.substring(6,8), dateRef.substring(8, 10), dateRef.substring(10, 12))
        let dateOut = `${date.getFullYear()}/${date.getMonth()}/${date.getDate()} at  ${date.getHours()}:${date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()}`
        let blogRow = document.createElement('tr')
        let blogTdL = document.createElement('td'); blogTdL.className = 'blogtdl'
        let blogTdR = document.createElement('td'); blogTdR.className = 'blogtdr'
        let blogEditButton = document.createElement('input'); blogEditButton.type = 'button'
        let blogDelButton = document.createElement('input'); blogDelButton.type = 'button'
        blogEditButton.value = 'Edit'; blogDelButton.value = 'Delete'
        blogEditButton.addEventListener('click', () => { ui.renderPopUp(ui.addBlog(c)); ui.addBlogFunctions(c) })
        blogDelButton.addEventListener('click', () => {
          if(confirm(`Are you sure you would like to delete "${c.title}"?`)) ajaxRequest(`req=delblog&id=${c.id}`)
        })
        blogTdL.innerHTML = `<b>${c.title}</b> posted on ${dateOut} by ${c.author}: `
        blogTdR.appendChild(blogEditButton)
        blogTdR.appendChild(blogDelButton)
        blogRow.appendChild(blogTdL); blogRow.appendChild(blogTdR)
        blogTable.appendChild(blogRow)
      }
    })}
    let blogRow = document.createElement('tr')
    let blogTdL = document.createElement('td'); blogTdL.className = 'blogtdl'
    let blogTdR = document.createElement('td'); blogTdR.className = 'blogtdr'
    let blogL = document.createElement('input'); blogL.type = 'button';
    let blogR = document.createElement('input'); blogR.type = 'button';
    blogL.value = '< Previous Page'; blogR.value = 'Next Page >'
    blogTdL.appendChild(blogL); blogTdR.appendChild(blogR)
    blogRow.appendChild(blogTdL); blogRow.appendChild(blogTdR)
    blogTable.appendChild(blogRow)
    blogL.addEventListener('click', () => ajaxRequest(`req=getblog&offset=${parseInt(jObj.offset) - 10}`))
    blogR.addEventListener('click', () => ajaxRequest(`req=getblog&offset=${parseInt(jObj.offset) + 10}`))
    this.elem.blogDiv.appendChild(blogTable)
  }
  renderResources(jObj){
    if(!document.getElementById('resourcesdiv')){
      const resourcesdiv = document.createElement('div'); resourcesdiv.id = 'resourcesdiv'
      document.getElementById('container').insertAdjacentElement('beforeend', resourcesdiv)
    } else{
      while(document.getElementById('resourcesdiv').lastChild){
        document.getElementById('resourcesdiv').removeChild(document.getElementById('resourcesdiv').lastChild)
      }
    }
    const bc = document.createElement('div')
    let home = document.createElement('a');
    if(this.resourcesParent == 0) home = document.createElement('span')
    home.innerText = 'Root'
    bc.insertAdjacentElement('afterbegin', home)
    if(this.resourcesParent != 0) home.onclick = ()=>{ ajaxRequest('req=getresources&parent=0'); this.resourcesBreadCrumb = [] }
    let i
    for(i=0; i<this.resourcesBreadCrumb.length; ++i){
      if(this.resourcesBreadCrumb[i]){
        if(i == this.resourcesParent){
          bc.insertAdjacentText('beforeend', ` > ${this.resourcesBreadCrumb[i]}`)
          let ii
          for(ii=1; i+ii<this.resourcesBreadCrumb.length; ii++){
            this.resourcesBreadCrumb[i+ii] = null
          }
        }else{
          const a = document.createElement('a')
          a.innerText = ` > ${this.resourcesBreadCrumb[i]}`
          bc.insertAdjacentElement('beforeend', a)
          a.onclick = (function(i){
            return function(){
              ajaxRequest(`req=getresources&parent=${i}`)
            }
          })(i)
        }
      }
    }
    document.getElementById('resourcesdiv').insertAdjacentHTML('beforeend', '<h3>Resource Management</h3>')
    document.getElementById('resourcesdiv').insertAdjacentElement('beforeend', bc)
    let addDiv = document.createElement('div'); let addButton = document.createElement('input')
    addDiv.className = 'createpostbg'; addButton.type = 'button'; addButton.value = 'Add Resource/Category'
    let rt = document.createElement('table')
    let rr = document.createElement('tr')
    let rc = document.createElement('td')
    rt.id = 'resourcestable'
    addDiv.appendChild(addButton); document.getElementById('resourcesdiv').insertAdjacentElement('beforeend', addDiv)
    addButton.onclick = ()=> {
      this.renderPopUp(this.editResource())
      this.addBlogFunctions()
    }
    if(jObj.resources.length > 0){
      jObj.resources.forEach((c)=>{
        rr = document.createElement('tr')
        rc = document.createElement('td')
        rc.innerText = `${c.title} `
        let rb = document.createElement('input'); rb.type = 'button'
        let re = document.createElement('input'); re.type = 'button'
        let rd = document.createElement('input'); rd.type = 'button'
        if(c.category) rb.value = '>'; else rb.value = 'v'
        re.value = 'Edit'; rd.value = 'Delete'
        rc.insertAdjacentElement('beforeend', rb)
        rc.insertAdjacentElement('beforeend', re)
        rc.insertAdjacentElement('beforeend', rd)
        const rcon = document.createElement('div')
        rcon.className = 'resourcecontent'; rcon.innerHTML = c.content
        rcon.style.display = 'none'
        rc.insertAdjacentElement('beforeend', rcon)
        rb.onclick = ()=>{
          if(c.category){
            ajaxRequest(`req=getresources&parent=${c.id}`)
            this.resourcesBreadCrumb[c.id] = c.title
          }else{
            if(rcon.style.display == 'none'){
              rcon.style.display = 'block'
              rb.value = '^'
            }else{
              rcon.style.display = 'none'
              rb.value = 'v'
            }
          }
        }
        re.onclick = ()=>{ this.renderPopUp(this.editResource(c)); this.addBlogFunctions() }
        rd.onclick = ()=>{
          if(confirm(`Are you sure you would like to delete '${c.title}?'`)) ajaxRequest(`req=delresource&id=${c.id}`)
        }
        rr.appendChild(rc)
        rt.insertAdjacentElement('beforeend', rr)
      })
    }else{
      rc.innerText = 'Nothing to show.'
      rr.appendChild(rc)
      rt.appendChild(rr)
    }
    document.getElementById('resourcesdiv').insertAdjacentElement('beforeend', rt)
    if((config.blogpost || config.admin) && !document.getElementById('blogdiv')) ajaxRequest('req=getblog&offset=0')
  }
  editResource(jObj){
    this.elem.cont = document.createElement('div'); this.elem.hold = document.createElement('div')
    this.elem.cont.id = 'popupcont'; this.elem.hold.id = 'popuphold'
    let title = document.createElement('input')
    let content = document.createElement('textarea')
    let add = document.createElement('input')
    let can = document.createElement('input')
    let preview = document.createElement('div')
    let adds = document.createElement('input'); let addcol = document.createElement('input')
    let addb = document.createElement('input'); let addi = document.createElement('input')
    let addc = document.createElement('input'); let addr = document.createElement('input')
    let addl = document.createElement('input'); let addu = document.createElement('input')
    let addcp = document.createElement('input')
    let file = document.createElement('input'); file.type = 'file'; file.id = 'bfile'
    let progress = document.createElement('span'); progress.id = 'bprog'
    let images = document.createElement('div'); images.id = 'bimagecont';
    let fileUp = document.createElement('input'); fileUp.type = 'button'
    let catBox = document.createElement('input'); catBox.type = 'checkbox'
    catBox.id = 'resourcecatcheck'
    if(jObj && jObj.category){
      catBox.checked = true
      content.disabled = true
      content.style.backgroundColor = '#8F8F8F'
    }
    file.setAttribute('multiple', 'multiple'); file.setAttribute('accept', 'image/jpeg, image/jpg, image/png, image/gif, image/tiff, image/tff')
    fileUp.value = 'Upload'; fileUp.id = 'bfileup'
    adds.className = 'bs'; addcol.className = 'bcol'
    addb.className = 'bb'; addi.className = 'bi'; addc.className = 'bc'
    addr.className = 'br'; addl.className = 'addl'; addu.className = 'bu'
    addb.id = 'addb'; addi.id = 'addi'; addc.id = 'addc'
    addr.id = 'addr'; addl.id = 'addl'; addu.id = 'addu'
    adds.id = 'adds'; addcol.id = 'addcol'; addcp.id = 'addcp'
    addb.type = 'button'; addi.type = 'button'; addc.type = 'button'
    addr.type = 'button'; addl.type = 'button'; addu.type = 'button'
    adds.type = 'button'; addcol.type = 'button'; addcp.type = 'button'
    addb.value = 'bold'; addi.value = 'italic'; addc.value = 'center'
    addr.value = 'right'; addl.value = 'hyperlink'; addu.value = 'underline'
    adds.value = 'size'; addcol.value = 'color'; addcp.value = 'preview'
    title.id = 'btitle'; content.id = 'bcontent'; preview.id = 'bpreview'
    add.type = 'button'; can.type = 'button'
    add.value = 'Submit Post'; can.value = 'Cancel'
    add.id = 'badd'; can.id = 'bcan'
    content.rows = '10';
    preview.innerHTML = '<center><i>Nothing to show (yet).</i></center>'
    if(jObj){
      title.setAttribute('value', jObj.title)
      content.innerHTML = jObj.content
      preview.innerHTML = jObj.content
    }
    content.setAttribute('onkeyup', "if(document.getElementById('bcontent').value != '') { document.getElementById('bpreview').innerHTML = document.getElementById('bcontent').value }else{ document.getElementById('bpreview').innerHTML = '<center><i>Nothing to show (yet).</i></center>' }")
    this.elem.hold.innerHTML = 'Resource Title: '
    this.elem.hold.appendChild(title); this.elem.hold.innerHTML += '<br />'
    this.elem.hold.appendChild(adds); this.elem.hold.appendChild(addcol)
    this.elem.hold.appendChild(addb); this.elem.hold.appendChild(addi); this.elem.hold.appendChild(addu)
    this.elem.hold.innerHTML += '┌───Resource Body───┐'
    this.elem.hold.appendChild(addc); this.elem.hold.appendChild(addr); this.elem.hold.appendChild(addl)
    this.elem.hold.appendChild(addcp)
    this.elem.hold.appendChild(content);
    this.elem.hold.innerHTML += '<br />Upload image(s): '
    this.elem.hold.appendChild(file); this.elem.hold.appendChild(fileUp); this.elem.hold.appendChild(progress)
    this.elem.imageContDisplay = true
    const showHideImages = document.createElement('input'); showHideImages.id = 'showhideimages'
    showHideImages.type = 'button'; showHideImages.value = 'Hide Images'
    this.elem.hold.appendChild(showHideImages)
    this.elem.hold.innerHTML += '<br />'
    this.elem.hold.appendChild(images)
    this.elem.hold.innerHTML += '<i><small>Click an image to add it to your post.</small></i>'
    this.elem.hold.innerHTML += '<br />┌───Post Body Preview───┐<br />'
    this.elem.hold.appendChild(preview)
    this.elem.hold.insertAdjacentText('beforeend','Category: ')
    this.elem.hold.insertAdjacentElement('beforeend', catBox)
    this.elem.hold.insertAdjacentElement('beforeend', document.createElement('br'))
    this.elem.hold.appendChild(add); this.elem.hold.appendChild(can)
    this.elem.cont.appendChild(this.elem.hold)
    add.onclick = () => this.submitResource(jObj)
    return this.elem.cont
  }
  submitResource(jObj){
    const title = document.getElementById('btitle')
    const cont = document.getElementById('bcontent')
    const category = document.getElementById('resourcecatcheck').checked ? 1 : 0
    if(!title.value.length) return alert('Blog title may not be blank.')
    if(!cont.value.length) return alert('Blog post body may not be blank.')
    ajaxRequest(`req=submitresource${jObj ? '&id=' + jObj.id : ''}&title=${title.value}&content=${cont.value}&category=${category}&parent=${this.resourcesParent}`)
  }

  renderBlog(){
    document.getElementById('container').insertAdjacentElement('beforeend', this.elem.blogDiv)
  }
  renderElements(){
    document.getElementById('container').appendChild(this.elem.settingsTable)
  }
  clearBlog(){
    if(document.getElementById('blogdiv')){
      while(document.getElementById('blogdiv').lastChild){
        document.getElementById('blogdiv').removeChild(document.getElementById('blogdiv').lastChild)
      }
    }
  }
  clearUI(){
    while(document.getElementById('container').lastChild){
      document.getElementById('container').removeChild(document.getElementById('container').lastChild)
    }
  }
  clearPopUp(){
    if(document.getElementById('popup')){
      while(document.getElementById('popup').lastChild){
        document.getElementById('popup').removeChild(document.getElementById('popup').lastChild);
      }
      this.elem.popUpBg.style.display = 'none'
    }
  }
  renderPopUp(el){
    this.elem.popUpBg.appendChild(this.elem.popUp)
    this.elem.popUpBg.id = 'popupbg'
    this.elem.popUp.id = 'popup'
    this.elem.popUp.onmousedown = () => { if(event.target.id == 'popup') this.clearPopUp() }
    document.getElementById('container').appendChild(this.elem.popUpBg)
    this.elem.popUp.appendChild(el)
    this.elem.popUpBg.style.display = 'table'
  }
  makeAPassPopUp(){
    this.elem.cont = document.createElement('div'); this.elem.hold = document.createElement('div')
    this.elem.cont.id = 'popupcont'; this.elem.hold.id = 'popuphold'
    this.elem.currentPass.id = 'currentpass'
    this.elem.passField1.id = 'passfield1'; this.elem.passField2.id = 'passfield2'
    this.elem.confirm.type = 'button'; this.elem.cancel.type = 'button'
    this.elem.confirm.value = 'Submit'; this.elem.cancel.value = 'Cancel'
    this.elem.confirm.onclick = () => this.submitAPass()
    this.elem.cancel.onclick = () => this.clearPopUp()
    this.elem.currentPass.type = 'password'
    this.elem.passField1.type = 'password'; this.elem.passField2.type = 'password'
    this.elem.hold.innerHTML = 'Enter current password: '; this.elem.hold.appendChild(this.elem.currentPass)
    this.elem.hold.innerHTML += '<br />Enter new password: '; this.elem.hold.appendChild(this.elem.passField1)
    this.elem.hold.innerHTML += '<br />Confirm new password: '; this.elem.hold.appendChild(this.elem.passField2)
    this.elem.hold.innerHTML += '<br />'
    this.elem.hold.appendChild(this.elem.confirm); this.elem.hold.appendChild(this.elem.cancel)
    this.elem.cont.appendChild(this.elem.hold)
    return this.elem.cont;
  }
  makeUserManagement(res){
    //'users'('id' INTEGER PRIMARY KEY, 'uname' TEXT, 'name' TEXT, 'pword' TEXT, 'blogpost' INTEGER, 'calendar' INTEGER, 'admin' INTEGER)
    //umTable: umRow: umCell: umEditButton: umDelButton: umAddButton: umUName: umPWord: umB: umC: umA:
    this.elem.cont = document.createElement('div'); this.elem.hold = document.createElement('div')
    this.elem.cont.id = 'popupcont'; this.elem.hold.id = 'popuphold'
    this.elem.umTable = document.createElement('table'); this.elem.umTable.id = 'usermantable'
    this.elem.umRow = document.createElement('tr')
    this.elem.umCell = document.createElement('td')
    this.elem.umCell.innerHTML = 'ID'
    this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.umCell.innerHTML = 'Username'
    this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.umCell.innerHTML = 'Name'
    this.elem.umRow.appendChild(this.elem.umCell)
    //this.elem.umCell = document.createElement('td')
    //this.elem.umCell.innerHTML = 'Password'
    //this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.umCell.innerHTML = 'Can blog?'
    this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.umCell.innerHTML = 'Manage calendar?'
    this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.umCell.innerHTML = 'Can announce?'
    this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.umCell.innerHTML = 'Is admin?'
    this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.umCell.innerHTML = 'Actions'
    this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umTable.appendChild(this.elem.umRow)
    res.user.forEach((c, i) => {
      this.elem.umRow = document.createElement('tr')
      this.elem.umCell = document.createElement('td')
      this.elem.umCell.innerText = res.user[i].id
      this.elem.umRow.appendChild(this.elem.umCell)
      this.elem.umCell = document.createElement('td')
      this.elem.umCell.innerText = res.user[i].uname
      this.elem.umRow.appendChild(this.elem.umCell)
      this.elem.umCell = document.createElement('td')
      this.elem.umCell.innerText = res.user[i].name
      this.elem.umRow.appendChild(this.elem.umCell)
      this.elem.umCell = document.createElement('td')
      this.elem.umCell.innerText = res.user[i].blogpost ? 'yes' : 'no'
      this.elem.umRow.appendChild(this.elem.umCell)
      this.elem.umCell = document.createElement('td')
      this.elem.umCell.innerText = res.user[i].calendar ? 'yes' : 'no'
      this.elem.umRow.appendChild(this.elem.umCell)
      this.elem.umCell = document.createElement('td')
      this.elem.umCell.innerText = res.user[i].announce ? 'yes' : 'no'
      this.elem.umRow.appendChild(this.elem.umCell)
      this.elem.umCell = document.createElement('td')
      this.elem.umCell.innerText = res.user[i].admin ? 'yes' : 'no'
      this.elem.umRow.appendChild(this.elem.umCell)
      this.elem.umCell = document.createElement('td')
      this.elem.umEditButton = document.createElement('input'); this.elem.umEditButton.value = 'edit'
      this.elem.umEditButton.onclick = () => { this.clearPopUp(); this.renderPopUp(this.addEditUser(true, c)) }
      this.elem.umDelButton = document.createElement('input'); this.elem.umDelButton.value = 'delete'
      this.elem.umDelButton.onclick = () => { this.delUser(res.user[i].id, res.user[i].uname) }
      this.elem.umEditButton.type = 'button'; this.elem.umDelButton.type = 'button'
      this.elem.umRow.appendChild(this.elem.umEditButton); this.elem.umRow.appendChild(this.elem.umDelButton)
      this.elem.umTable.appendChild(this.elem.umRow)
    })
    this.elem.hold.appendChild(this.elem.umTable)
    this.elem.umAddButton.type = 'button'; this.elem.umAddButton.value = 'Add User'
    this.elem.umAddButton.onclick = () => { this.clearPopUp(); this.renderPopUp(this.addEditUser(false)) }
    this.elem.hold.appendChild(this.elem.umAddButton);
    this.elem.cont.appendChild(this.elem.hold)
    return this.elem.cont
  }
  makeCalManagement(res){
    function getNewMonth(direction){
      let newYear = 0
      let newMonth = 0
      if(direction){
        newYear = res.month == 12 ? parseInt(res.year) + 1 : res.year
        newMonth = res.month == 12 ? 1 : parseInt(res.month) + 1
      } else {
        newYear = res.month == 1 ? parseInt(res.year) - 1 : res.year
        newMonth = res.month == 1 ? 12 : parseInt(res.month) - 1
      }
      ajaxRequest(`req=getcal&year=${newYear}&month=${newMonth}`)
    }
    this.elem.cont = document.createElement('div'); this.elem.hold = document.createElement('div')
    this.elem.cont.id = 'popupcont'; this.elem.hold.id = 'popuphold'
    let add = document.createElement('input'); add.type = 'button'; add.value = 'Add'
    let prev = document.createElement('input'); prev.type = 'button'; prev.value = '<'
    let next = document.createElement('input'); next.type = 'button'; next.value = '>'
    add.setAttribute('onclick', `ui.clearPopUp(); ui.renderPopUp(ui.addCal(${res.year}, ${res.month}))`)
    prev.onclick = () => getNewMonth(0)
    next.onclick = () => getNewMonth(1)
    this.elem.hold.appendChild(prev)
    this.elem.hold.appendChild(document.createTextNode(`${months[res.month]} ${res.year}`))
    this.elem.hold.appendChild(next)
    if(res.events){
      res.events.forEach((c) => {
        let d = new Date(c.year, c.month-1, c.day)
        let row = document.createElement('div')
        let edit = document.createElement('input')
        let del = document.createElement('input')
        const dayText = function(day){
          const num = day.toString().charAt(day.toString().length-1)
          if(num == '1' && (day < 4 || day > 20)) { return day + 'st' }
          else if(num == '2' && (day < 4 || day > 20)) { return day + 'nd' }
          else if(num == '3' && (day < 4 || day > 20)) { return day + 'rd' }
          else return day + 'th'
        }
        row.className = 'calrow'
        edit.type = 'button'; del.type = 'button'
        edit.value = 'edit'; del.value = 'delete'
        edit.onclick = () => { this.clearPopUp(); this.renderPopUp(this.addCal(c.year, c.month, c)) }
        del.onclick = () => { if(confirm(`Are you sure you would like to delete "${c.title}" from the ${dayText(c.day)}?`)) ajaxRequest(`req=delcal&id=${c.id}&year=${c.year}&month=${c.month}`) }
        row.appendChild(document.createTextNode(`${days[d.getDay() + 1]} the ${dayText(c.day)}, ${c.hour < 13 ? c.hour : c.hour - 12}:${c.minute < 10 ? '0' + c.minute : c.minute}${c.hour < 12 ? 'am' : 'pm'} - ${c.title}`))
        row.appendChild(edit); row.appendChild(del)
        this.elem.hold.appendChild(row)
      })
    } else {
      let row = document.createElement('div')
      row.className = 'calrow'
      row.innerHTML = 'No events to display.'
      this.elem.hold.appendChild(row)
    }
    this.elem.hold.appendChild(add)
    this.elem.cont.appendChild(this.elem.hold)
    return this.elem.cont;
  }
  addCal(year, month, ev){
    function submit(){
      if(document.getElementById('actitle').value != ''){
        let req = 'req=addcal'
        const acday = document.getElementById('acday').value
        const achour = document.getElementById('achour').value
        const acminute = document.getElementById('acminute').value
        const zeroCheck = (val) => {
          if(val < 10) return `0${val}`
          else return val
        }
        if(ev) req+= `&id=${ev.id}`
        req += `&title=${document.getElementById('actitle').value}`
        req += `&desc=${document.getElementById('acdesc').value}`
        req += `&year=${year}`
        req += `&month=${month}`
        req += `&day=${acday}`
        req += `&hour=${achour}`
        req += `&minute=${acminute}`
        req += `&date=${year}${zeroCheck(month)}${zeroCheck(acday)}${zeroCheck(achour)}${zeroCheck(acminute)}`
        ajaxRequest(req)
      } else alert('The title field may not be left empty.')
    }
    this.elem.cont = document.createElement('div'); this.elem.hold = document.createElement('div')
    this.elem.cont.id = 'popupcont'; this.elem.hold.id = 'popuphold'
    let day = document.createElement('select')
    let hour = document.createElement('select')
    let minute = document.createElement('select')
    let title = document.createElement('input')
    let desc = document.createElement('textarea')
    let sub = document.createElement('input')
    let can = document.createElement('input')
    let leap = 0
    if(month==2)
    {
      if(year % 4 == 0)
      {
        leap = 1;
        if(year % 100 == 0)
        {
          leap = 0;
          if(year % 400 == 0)
          {
            leap = 1;
          }
        }
      }
    }
    const numDays = leap && month == 2 ? monthdays[month] + 1 : monthdays[month];
    let i = 0
    day.setAttribute('id', 'acday')
    for(i=1; i<numDays+1; i++){
      const d = new Date(year, month-1, i)
      let option = document.createElement('option')
      option.value = i; option.innerText = `${i} - ${days[d.getDay() + 1]}`
      if(ev && i == ev.day) option.setAttribute('selected', 'selected')
      day.appendChild(option)
    }
    hour.setAttribute('id', 'achour')
    for(i=0; i<24; i++){
      let option = document.createElement('option')
      option.value = i; option.innerText = i
      if(ev && i == ev.hour) option.setAttribute('selected', 'selected')
      hour.appendChild(option)
    }
    minute.setAttribute('id','acminute')
    for(i=0; i<60; i+=5){
      let option = document.createElement('option')
      if(i<10){ option.value = i; option.innerText = '0' + i }
      else { option.value = i; option.innerText = i }
      if(ev && i == ev.minute) option.setAttribute('selected', 'selected')
      minute.appendChild(option)
    }
    title.type = 'text'; title.setAttribute('id', 'actitle')
    title.style.width = '75%'
    desc.setAttribute('rows', '5'); desc.setAttribute('id', 'acdesc')
    desc.style.width = '100%'
    if(ev){ title.setAttribute('value', ev.title); desc.innerText = ev.desc }
    sub.type = 'button'; can.type = 'button'
    sub.value = 'Submit'; can.value = 'Cancel'
    sub.onclick = () => { submit() };
    can.onclick = () => { this.clearPopUp(); ajaxRequest(`req=getcal&year=${year}&month=${month}`) }
    this.elem.hold.innerHTML = `Event in ${months[month]} ${year}`
    this.elem.hold.innerHTML += '<br />Day: '; this.elem.hold.appendChild(day)
    this.elem.hold.innerHTML += ' Hour: '; this.elem.hold.appendChild(hour)
    this.elem.hold.innerHTML += ' Minute: '; this.elem.hold.appendChild(minute)
    this.elem.hold.innerHTML += '<br />Event Title: '; this.elem.hold.appendChild(title)
    this.elem.hold.innerHTML += '<br />┌───Event Description───┐<br />'; this.elem.hold.appendChild(desc)
    this.elem.hold.innerHTML += '<br />'
    this.elem.hold.appendChild(sub); this.elem.hold.appendChild(can)
    this.elem.cont.appendChild(this.elem.hold)
    return this.elem.cont
  }
  addEditUser(edit, user){
    this.elem.cont = document.createElement('div'); this.elem.hold = document.createElement('div')
    this.elem.cont.id = 'popupcont'; this.elem.hold.id = 'popuphold'
    this.elem.umTable = document.createElement('table'); this.elem.umTable.id = 'usermantable'
    this.elem.umRow = document.createElement('tr')
    this.elem.umCell = document.createElement('td')
    this.elem.umCell.innerHTML = 'Username'
    this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.umCell.innerHTML = 'Name'
    this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.umCell.innerHTML = 'Password'
    this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.umCell.innerHTML = 'Confirm password'
    this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.umCell.innerHTML = 'Can blog?'
    this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.umCell.innerHTML = 'Manage calendar?'
    this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.umCell.innerHTML = 'Can announce?'
    this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.umCell.innerHTML = 'Is admin?'
    this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umTable.appendChild(this.elem.umRow)
    this.elem.umRow = document.createElement('tr')
    this.elem.umCell = document.createElement('td')
    this.elem.uaeInput = document.createElement('input'); this.elem.uaeInput.type = 'text'
    this.elem.uaeInput.setAttribute('value',  edit ? user.uname : '')
    this.elem.uaeInput.setAttribute('id', 'uaeuname')
    this.elem.umCell.appendChild(this.elem.uaeInput); this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.uaeInput = document.createElement('input'); this.elem.uaeInput.type = 'text'
    this.elem.uaeInput.setAttribute('value', edit ? user.name : '')
    this.elem.uaeInput.setAttribute('id', 'uaename')
    this.elem.umCell.appendChild(this.elem.uaeInput); this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.uaeInput = document.createElement('input'); this.elem.uaeInput.type = 'password'
    this.elem.uaeInput.setAttribute('id', 'uaepass1')
    this.elem.umCell.appendChild(this.elem.uaeInput); this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.uaeInput = document.createElement('input'); this.elem.uaeInput.type = 'password'
    this.elem.uaeInput.setAttribute('id', 'uaepass2')
    this.elem.umCell.appendChild(this.elem.uaeInput); this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.uaeInput = document.createElement('input'); this.elem.uaeInput.type = 'checkbox'
    if(edit && user.blogpost) this.elem.uaeInput.setAttribute('checked', 'checked')
    this.elem.uaeInput.setAttribute('id', 'uaeblogpost')
    this.elem.umCell.appendChild(this.elem.uaeInput); this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.uaeInput = document.createElement('input'); this.elem.uaeInput.type = 'checkbox'
    if(edit && user.calendar) this.elem.uaeInput.setAttribute('checked', 'checked')
    this.elem.uaeInput.setAttribute('id', 'uaecalendar')
    this.elem.umCell.appendChild(this.elem.uaeInput); this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.uaeInput = document.createElement('input'); this.elem.uaeInput.type = 'checkbox'
    if(edit && user.announce) this.elem.uaeInput.setAttribute('checked', 'checked')
    this.elem.uaeInput.setAttribute('id', 'uaeannounce')
    this.elem.umCell.appendChild(this.elem.uaeInput); this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umCell = document.createElement('td')
    this.elem.uaeInput = document.createElement('input'); this.elem.uaeInput.type = 'checkbox'
    if(edit && user.admin) this.elem.uaeInput.setAttribute('checked', 'checked')
    this.elem.uaeInput.setAttribute('id', 'uaeadmin')
    this.elem.umCell.appendChild(this.elem.uaeInput); this.elem.umRow.appendChild(this.elem.umCell)
    this.elem.umTable.appendChild(this.elem.umRow)
    this.elem.hold.appendChild(this.elem.umTable)
    this.elem.uaeInput = document.createElement('input'); this.elem.uaeInput.type = 'button'
    this.elem.uaeInput.value = 'Submit'
    this.elem.uaeInput.setAttribute('onclick', edit ? `javascript: ui.addUser(false, ${user.id})` : 'javascript: ui.addUser(true)')
    this.elem.hold.appendChild(this.elem.uaeInput)
    this.elem.uaeInput = document.createElement('input'); this.elem.uaeInput.type = 'button'
    this.elem.uaeInput.value = 'Cancel'
    this.elem.uaeInput.setAttribute('onclick', 'javascript: ui.clearPopUp(); ajaxRequest("req=getusers")')
    this.elem.hold.appendChild(this.elem.uaeInput)
    this.elem.hold.innerHTML += '<br />Leave the password fields blank if you do not wish to update the user\'s password.<br />If a user is admin, they will be able to manage ALL blog posts and the calendar.<br />Only someone logged in to the "admin" account can change the admin password.<br />Anybody who is marked as "admin" can manage users.<br />Non-admin users can only manage their own blog posts, if "can blog" is enabled for them.'
    this.elem.cont.appendChild(this.elem.hold)
    return this.elem.cont
  }
  editAnnouncement(jObj){
    this.elem.cont = document.createElement('div'); this.elem.hold = document.createElement('div')
    this.elem.cont.id = 'popupcont'; this.elem.hold.id = 'popuphold'
    let content = document.createElement('textarea')
    let type = document.createElement('select')
    let hidden = document.createElement('option'); let pop = document.createElement('option'); let stat =  document.createElement('option')
    let add = document.createElement('input')
    let can = document.createElement('input')
    let preview = document.createElement('div')
    let adds = document.createElement('input'); let addcol = document.createElement('input')
    let addb = document.createElement('input'); let addi = document.createElement('input')
    let addc = document.createElement('input'); let addr = document.createElement('input')
    let addl = document.createElement('input'); let addu = document.createElement('input')
    let addcp = document.createElement('input')
    let file = document.createElement('input'); file.type = 'file'; file.id = 'bfile'
    let progress = document.createElement('span'); progress.id = 'bprog'
    let images = document.createElement('div'); images.id = 'bimagecont'
    file.setAttribute('multiple', 'multiple'); file.setAttribute('accept', 'image/jpeg, image/jpg, image/png, image/gif, image/tiff, image/tff')
    let fileUp = document.createElement('input'); fileUp.type = 'button'
    fileUp.value = 'Upload'; fileUp.id = 'bfileup'
    addb.className = 'bb'; addi.className = 'bi'; addc.className = 'bc'
    addr.className = 'br'; addl.className = 'addl'; addu.className = 'bu'
    adds.classname = 'bs'; addcol.className = 'bcol'
    addb.id = 'addb'; addi.id = 'addi'; addc.id = 'addc'
    addr.id = 'addr'; addl.id = 'addl'; addu.id = 'addu'
    adds.id = 'adds'; addcol.id = 'addcol'; addcp.id = 'addcp'
    addb.type = 'button'; addi.type = 'button'; addc.type = 'button'
    addr.type = 'button'; addl.type = 'button'; addu.type = 'button'
    adds.type = 'button'; addcol.type = 'button'; addcp.type = 'button'
    addb.value = 'bold'; addi.value = 'italic'; addc.value = 'center'
    addr.value = 'right'; addl.value = 'hyperlink'; addu.value = 'underline'
    adds.value = 'size'; addcol.value = 'color'; addcp.value = 'preview'
    content.id = 'bcontent'; preview.id = 'bpreview'
    type.id = 'antype'
    hidden.value = 'hidden'; pop.value = 'popup'; stat.value = 'static'
    if(jObj.antype == 'hidden') hidden.selected = 'selected'
    if(jObj.antype == 'popup') pop.selected = 'selected'
    if(jObj.antype == 'static') stat.selected = 'selected'
    hidden.innerText = 'Hidden'; pop.innerText = 'Popup'; stat.innerText = 'Static'
    type.append(hidden, pop, stat)
    add.type = 'button'; can.type = 'button'
    add.value = 'Submit Announcement'; can.value = 'Cancel'
    add.id = 'badd'; can.id = 'bcan'
    content.rows = '10';
    preview.innerHTML = '<center><i>Nothing to show (yet).</i></center>'
    if(jObj){
      content.innerHTML = jObj.ancontent
      preview.innerHTML = jObj.ancontent
    }
    content.setAttribute('onkeyup', "if(document.getElementById('bcontent').value != '') { document.getElementById('bpreview').innerHTML = document.getElementById('bcontent').value }else{ document.getElementById('bpreview').innerHTML = '<center><i>Nothing to show (yet).</i></center>' }")
    this.elem.hold.appendChild(adds); this.elem.hold.appendChild(addcol)
    this.elem.hold.appendChild(addb); this.elem.hold.appendChild(addi); this.elem.hold.appendChild(addu)
    this.elem.hold.innerHTML += '┌───Announcement Body───┐'
    this.elem.hold.appendChild(addc); this.elem.hold.appendChild(addr); this.elem.hold.appendChild(addl)
    this.elem.hold.appendChild(addcp)
    this.elem.hold.appendChild(content);
    this.elem.hold.innerHTML += '<br />Upload image(s): '
    this.elem.hold.appendChild(file); this.elem.hold.appendChild(fileUp); this.elem.hold.appendChild(progress)
    this.elem.imageContDisplay = true
    const showHideImages = document.createElement('input'); showHideImages.id = 'showhideimages'
    showHideImages.type = 'button'; showHideImages.value = 'Hide Images'
    this.elem.hold.appendChild(showHideImages)
    this.elem.hold.innerHTML += '<br />'
    this.elem.hold.appendChild(images)
    this.elem.hold.innerHTML += '<i><small>Click an image to add it to the announcement.</small></i>'
    this.elem.hold.innerHTML += '<br />┌───Announcement Preview───┐<br />'
    this.elem.hold.appendChild(preview)
    this.elem.hold.insertAdjacentHTML('beforeend', '<small>Announcement type:</small>')
    this.elem.hold.appendChild(type); this.elem.hold.insertAdjacentElement('beforeend',document.createElement('br'))
    this.elem.hold.appendChild(add); this.elem.hold.appendChild(can)
    this.elem.cont.appendChild(this.elem.hold)
    this.renderPopUp(this.elem.cont)
    this.addBlogFunctions()
    add.onclick = this.submitAnnouncement
  }
  addBlog(jObj){
    this.elem.cont = document.createElement('div'); this.elem.hold = document.createElement('div')
    this.elem.cont.id = 'popupcont'; this.elem.hold.id = 'popuphold'
    let title = document.createElement('input')
    let content = document.createElement('textarea')
    let add = document.createElement('input')
    let can = document.createElement('input')
    let preview = document.createElement('div')
    let adds = document.createElement('input'); let addcol = document.createElement('input')
    let addb = document.createElement('input'); let addi = document.createElement('input')
    let addc = document.createElement('input'); let addr = document.createElement('input')
    let addl = document.createElement('input'); let addu = document.createElement('input')
    let addcp = document.createElement('input')
    let file = document.createElement('input'); file.type = 'file'; file.id = 'bfile'
    let progress = document.createElement('span'); progress.id = 'bprog'
    let images = document.createElement('div'); images.id = 'bimagecont'
    file.setAttribute('multiple', 'multiple'); file.setAttribute('accept', 'image/jpeg, image/jpg, image/png, image/gif, image/tiff, image/tff')
    let fileUp = document.createElement('input'); fileUp.type = 'button'
    fileUp.value = 'Upload'; fileUp.id = 'bfileup'
    adds.className = 'bs'; addcol.className = 'bcol'
    addb.className = 'bb'; addi.className = 'bi'; addc.className = 'bc'
    addr.className = 'br'; addl.className = 'addl'; addu.className = 'bu'
    addb.id = 'addb'; addi.id = 'addi'; addc.id = 'addc'
    addr.id = 'addr'; addl.id = 'addl'; addu.id = 'addu'
    adds.id = 'adds'; addcol.id = 'addcol'; addcp.id = 'addcp'
    addb.type = 'button'; addi.type = 'button'; addc.type = 'button'
    addr.type = 'button'; addl.type = 'button'; addu.type = 'button'
    adds.type = 'button'; addcol.type = 'button'; addcp.type = 'button'
    addb.value = 'bold'; addi.value = 'italic'; addc.value = 'center'
    addr.value = 'right'; addl.value = 'hyperlink'; addu.value = 'underline'
    adds.value = 'size'; addcol.value = 'color'; addcp.value = 'preview'
    title.id = 'btitle'; content.id = 'bcontent'; preview.id = 'bpreview'
    add.type = 'button'; can.type = 'button'
    add.value = 'Submit Post'; can.value = 'Cancel'
    add.id = 'badd'; can.id = 'bcan'
    content.rows = '10';
    preview.innerHTML = '<center><i>Nothing to show (yet).</i></center>'
    if(jObj){
      title.setAttribute('value', jObj.title)
      content.innerHTML = jObj.content
      preview.innerHTML = jObj.content
    }
    content.setAttribute('onkeyup', "if(document.getElementById('bcontent').value != '') { document.getElementById('bpreview').innerHTML = document.getElementById('bcontent').value }else{ document.getElementById('bpreview').innerHTML = '<center><i>Nothing to show (yet).</i></center>' }")
    this.elem.hold.innerHTML = 'Post Title: '
    this.elem.hold.appendChild(title); this.elem.hold.innerHTML += '<br />'
    this.elem.hold.appendChild(adds); this.elem.hold.appendChild(addcol)
    this.elem.hold.appendChild(addb); this.elem.hold.appendChild(addi); this.elem.hold.appendChild(addu)
    this.elem.hold.innerHTML += '┌───Post Body───┐'
    this.elem.hold.appendChild(addc); this.elem.hold.appendChild(addr); this.elem.hold.appendChild(addl)
    this.elem.hold.appendChild(addcp)
    this.elem.hold.appendChild(content);
    this.elem.hold.innerHTML += '<br />Upload image(s): '
    this.elem.hold.appendChild(file); this.elem.hold.appendChild(fileUp); this.elem.hold.appendChild(progress)
    this.elem.imageContDisplay = true
    const showHideImages = document.createElement('input'); showHideImages.id = 'showhideimages'
    showHideImages.type = 'button'; showHideImages.value = 'Hide Images'
    this.elem.hold.appendChild(showHideImages)
    this.elem.hold.innerHTML += '<br />'
    this.elem.hold.appendChild(images)
    this.elem.hold.innerHTML += '<i><small>Click an image to add it to your post.</small></i>'
    this.elem.hold.innerHTML += '<br />┌───Post Body Preview───┐<br />'
    this.elem.hold.appendChild(preview)
    this.elem.hold.appendChild(add); this.elem.hold.appendChild(can)
    this.elem.cont.appendChild(this.elem.hold)
    add.onclick = () => this.submitBlog(jObj ? jObj : null)
    return this.elem.cont
  }
  addBlogFunctions(){
    ajaxRequest('req=getimages')
    function addTags(tagStart, tagEnd){
      let el = document.getElementById('bcontent')
      el.value = `${el.value.substring(0, el.selectionStart)}${tagStart}${el.value.substring(el.selectionStart, el.selectionEnd)}${tagEnd}${el.value.substring(el.selectionEnd, el.length)}`
      document.getElementById('bpreview').innerHTML = el.value
      el.focus()
      el.selectionEnd += tagStart.length + tagEnd.length
      el.selectionStart = el.selectionEnd
    }
    function addLink(){
      let tagStart = ''
      let tagEnd = '</a>'
      let el = document.getElementById('bcontent')
      let prompt = window.prompt('Please insert the URL you would like to link to here:')
      if(prompt == null || prompt == '') return;
      tagStart = `<a href="${prompt}" target="_blank">`
      el.value = `${el.value.substring(0, el.selectionStart)}${tagStart}${el.value.substring(el.selectionStart, el.selectionEnd)}${tagEnd}${el.value.substring(el.selectionEnd, el.length)}`
      document.getElementById('bpreview').innerHTML = el.value
      el.focus()
      el.selectionEnd += tagStart.length + tagEnd.length
      el.selectionStart = el.selectionEnd
    }
    function addSize(){
      let tagStart = ''
      let tagEnd = '</font>'
      let el = document.getElementById('bcontent')
      let prompt = window.prompt('Please insert the font size you would like:', '3')
      if(prompt == null || prompt == '') return;
      tagStart = `<font size="${prompt}">`
      el.value = `${el.value.substring(0, el.selectionStart)}${tagStart}${el.value.substring(el.selectionStart, el.selectionEnd)}${tagEnd}${el.value.substring(el.selectionEnd, el.length)}`
      document.getElementById('bpreview').innerHTML = el.value
      el.focus()
      el.selectionEnd += tagStart.length + tagEnd.length
      el.selectionStart = el.selectionEnd
    }
    function addColor(){
      let tagStart = ''
      let tagEnd = '</font>'
      let el = document.getElementById('bcontent')
      let prompt = window.prompt('Please insert the font color you would like:', 'white')
      if(prompt == null || prompt == '') return;
      tagStart = `<font color="${prompt}">`
      el.value = `${el.value.substring(0, el.selectionStart)}${tagStart}${el.value.substring(el.selectionStart, el.selectionEnd)}${tagEnd}${el.value.substring(el.selectionEnd, el.length)}`
      document.getElementById('bpreview').innerHTML = el.value
      el.focus()
      el.selectionEnd += tagStart.length + tagEnd.length
      el.selectionStart = el.selectionEnd
    }
    function uploadFiles(){
      let ajax = new XMLHttpRequest()
      let file = document.getElementById('bfile').files
      let i = 0
      for(i=0; i<file.length; i++){
        if(file[i].size > 10000000){ //10mb
          return alert(`${file[i].name} is too large at ${file[i].size / 1000}mb. Maximum file size is 10mb.`)
        }
      }
      ajax.upload.addEventListener('progress', function(prog){
        document.getElementById('bprog').innerText = `Upload progress: ${(prog.loaded/prog.total) * 100}%`
      })
      ajax.onreadystatechange = function(){
        if(this.status == 200 && this.readyState == 4){
          console.log(this.responseText)
          let res = JSON.parse(this.responseText)
          if(res.type == 'message' && res.subject == 'uploadsuccess'){
            document.getElementById('bfile').value = ''
            document.getElementById('bprog').innerText = 'Upload progress: complete!'
            res.images.forEach(function(c){
              const bc = document.getElementById('bcontent')
              const bp = document.getElementById('bpreview')
              let img = document.createElement('img')
              img.src = c;
              document.getElementById('bimagecont').prepend(img)
              img.onclick = () => {
                let scale = prompt('Please choose a percentage of the width of the post that you would like this image to occupy:', '100')
                let tag = `<img src='${c}' style='width:${scale}%;height:auto;'>`
                bc.value = `${bc.value.substring(0, bc.selectionStart)}${tag}${bc.value.substring(bc.selectionEnd, bc.length)}`
                bp.innerHTML = bc.value
                bc.focus()
                bc.selectionEnd += tag.length
                bc.selectionStart = bc.selectionEnd
              }
            })
          } else alert('There was a server error when attempting to upload your file(s). Please try again.')
        }
      }
      let form = new FormData()
      for(i=0; i<file.length; i++){
        form.append('file[]', file[i])
      }
      form.append('req', 'upload')
      ajax.open('POST', 'hsocadmin.php')
      ajax.send(form)
    }
    document.getElementById('adds').onclick = () => addSize()
    document.getElementById('addcol').onclick = () => addColor()
    document.getElementById('addb').onclick = () => addTags('<b>', '</b>')
    document.getElementById('addi').onclick = () => addTags('<i>', '</i>')
    document.getElementById('addu').onclick = () => addTags('<u>', '</u>')
    document.getElementById('addc').onclick = () => addTags('<center>', '</center>')
    document.getElementById('addr').onclick = () => addTags('<span class="bright">', '</span>')
    document.getElementById('addl').onclick = () => addLink()
    document.getElementById('addcp').onclick = () => addTags('<span class="preview">', '</span>')
    document.getElementById('bfileup').onclick = () => uploadFiles()
    document.getElementById('bcan').onclick = () => this.clearPopUp()
    document.getElementById('showhideimages').onclick = ()=>{
      console.log('Show/hide: ' + document.getElementById('bimagecont').style.display)
      if(this.elem.imageContDisplay == true){
        console.log('setting to none')
        document.getElementById('bimagecont').style.display = 'none'
        document.getElementById('showhideimages').value = 'Show Images'
        this.elem.imageContDisplay = false
      }else{
        console.log('setting to block')
        document.getElementById('bimagecont').style.display = 'block'
        document.getElementById('showhideimages').value = 'Hide Images'
        this.elem.imageContDisplay = true
      }
    }
    if(document.getElementById('resourcecatcheck')){
      document.getElementById('resourcecatcheck').onchange = function(){
        if(document.getElementById('resourcecatcheck').checked){
          document.getElementById('bcontent').disabled = true
          document.getElementById('bcontent').style.backgroundColor = '#8F8F8F'
        }else{
          document.getElementById('bcontent').disabled = false
          document.getElementById('bcontent').style.backgroundColor = '#FFF'
        }
      }
    }
  }
  submitAnnouncement(){
    const cont = document.getElementById('bcontent')
    const type = document.getElementById('antype').value
    if(!cont.value.length) return alert('Announcement body may not be blank.')
    ajaxRequest(`req=submitannouncement&content=${cont.value}&type=${type}`)
  }
  submitBlog(jObj){
    const title = document.getElementById('btitle')
    const cont = document.getElementById('bcontent')
    if(!title.value.length) return alert('Blog title may not be blank.')
    if(!cont.value.length) return alert('Blog post body may not be blank.')
    ajaxRequest(`req=submitblog${jObj ? '&id=' + jObj.id : ''}&title=${title.value}&content=${cont.value}`)
  }
  submitAPass(){
    const cp = document.getElementById('currentpass').value
    const p1 = document.getElementById('passfield1').value
    const p2 = document.getElementById('passfield2').value
    if(cp.length > 0 && p1.length > 0 && p2.length > 0){
      if(p1 == p2){
        if(p1.length > 7){
          ajaxRequest(`req=setapass&newpass=${p1}&oldpass=${cp}`)
        }else{
          alert('New password must be 8 or more characters.')
        }
      }else{
        alert('Confirmation password does not match new password.')
      }
    }else{
      alert('All fields must be filled out.')
    }
  }
  addUser(add, id){
    if(document.getElementById('uaeuname').value.length < 3) return alert('Username must be 3 or more characters.');
    if(document.getElementById('uaename').value.length < 3) return alert('Name must be 3 or more characters.');
    let req = `req=addedituser&uname=${document.getElementById('uaeuname').value}`
      req += `&name=${document.getElementById('uaename').value}`
      req += `&blogpost=${document.getElementById('uaeblogpost').checked == true ? 1 : 0}`
      req += `&calendar=${document.getElementById('uaecalendar').checked == true ? 1 : 0}`
      req += `&announce=${document.getElementById('uaeannounce').checked == true ? 1 : 0}`
      req += `&admin=${document.getElementById('uaeadmin').checked == true ? 1 : 0}`
      if(!add) req += `&id=${id}`
      if(add && document.getElementById('uaepass1').value.length < 1) return alert('You must include a password when adding a user.')
      if(document.getElementById('uaepass1').value != ''){
        if(document.getElementById('uaepass1').value.length > 6){
          if(document.getElementById('uaepass1').value == document.getElementById('uaepass2').value){
            req += `&pword=${document.getElementById('uaepass1').value}`
          }else{
            return alert('Passwords do not match.')
          }
        }else{
          return alert('Password must be 8 or more characters.')
        }
      }
    ajaxRequest(req)
  }
  delUser(id, uname){
    if(confirm(`Are you sure you would like to delete ${uname}?`) == true){
      ajaxRequest(`req=deluser&id=${id}`)
    }
  }
}

const ui = new settingsUI()
const config = { apass: '', blogpost: 0, calendar: 0, admin: 0, resources: 0 }
const months = ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
const monthdays = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
const days = ["", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]

document.addEventListener("DOMContentLoaded", function(){ ajaxRequest('req=init') })

function ajaxRequest(req){
  const ajax = new XMLHttpRequest()
  ajax.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      console.log(`Response: ${this.responseText}`)
      const res = JSON.parse(this.responseText)
      if(res.type == 'init'){
        config.apass = res.apass
        config.id = res.id
        config.uname = res.uname
        config.calendar = res.calendar
        config.blogpost = res.blogpost
        config.admin = res.admin
        ui.makeElements()
        ui.clearUI()
        ui.renderElements()
        ajaxRequest('req=getresources&parent=0')
      }
      if(res.type == 'getblog'){
        ui.makeBlogElements(res)
        ui.renderBlog()
      }
      if(res.type == 'getresources'){
        ui.resourcesParent = res.parent
        ui.renderResources(res)
      }
      if(res.type == 'getusers'){
        ui.renderPopUp(ui.makeUserManagement(res))
      }
      if(res.type == 'getcal'){
        ui.clearPopUp()
        ui.renderPopUp(ui.makeCalManagement(res))
      }
      if(res.type == 'message'){
        if(res.subject == 'apasssuccess'){
          alert(res.message)
          ui.clearPopUp()
        }else if(res.subject == 'apassfail'){
          alert(res.message)
        }else if(res.subject == 'addusersuccess'){
          alert(res.message)
          ui.clearPopUp()
          ajaxRequest('req=getusers')
        } else if(res.subject == 'adduserfail'){
          alert(res.message)
        } else if (res.subject == 'delusersuccess'){
          alert(res.message)
          ui.clearPopUp()
          ajaxRequest('req=getusers')
        } else if (res.subject == 'addcalsuccess'){
          alert(res.message)
          ui.clearPopUp()
          ajaxRequest(`req=getcal&year=${res.year}&month=${res.month}`)
        } else if (res.subject == 'delcalsuccess'){
          alert(res.message)
          ui.clearPopUp()
          ajaxRequest(`req=getcal&year=${res.year}&month=${res.month}`)
        } else if(res.subject == 'submitblogsuccess'){
          ui.clearPopUp()
          ajaxRequest('req=getblog&offset=0')
          alert(res.message)
        } else if(res.subject == 'submitblogfail'){
          alert(res.message)
        } else if(res.subject == 'delblogsuccess'){
          alert(res.message)
          ajaxRequest('req=getblog&offset=0')
        } else if(res.subject == 'permissionfail'){
          alert(res.message)
        } else if(res.subject == 'submitresourcesuccess'){
          alert(res.message)
          ui.clearPopUp()
          ajaxRequest(`req=getresources&parent=${ui.resourcesParent}`)
        } else if(res.subject == 'delresourcesuccess'){
          alert(res.message)
          ajaxRequest(`req=getresources&parent=${ui.resourcesParent}`)
        } else if(res.subject == 'delresourcefail'){
          alert(res.message)
        }else{
          alert(res.message)
        }
      }
      if(res.type == 'getimages'){
        const ic = document.getElementById('bimagecont')
        if(ic){
          res.images.forEach(function(c){
            if(c != '.' && c != '..' && c != '.DS_Store'){
              const bc = document.getElementById('bcontent')
              const bp = document.getElementById('bpreview')
              let img = document.createElement('img')
              img.src = `uploads/${c}`
              ic.appendChild(img)
              img.onclick = () => {
                let scale = prompt('Please choose a percentage of the width of the post that you would like this image to occupy:', '100')
                let tag = `<img src='uploads/${c}' style='width:${scale}%;height:auto;'>`
                bc.value = `${bc.value.substring(0, bc.selectionStart)}${tag}${bc.value.substring(bc.selectionEnd, bc.length)}`
                bp.innerHTML = bc.value
                bc.focus()
                bc.selectionEnd += tag.length
                bc.selectionStart = bc.selectionEnd
              }
            }
          })
        }
      }
      if(res.type == 'getannouncement'){
        ui.clearPopUp()
        ui.editAnnouncement(res)
      }
      if(res.type == 'submitannouncement'){
        alert(res.message)
        ui.clearPopUp()
      }
    }
  }
  ajax.open('POST', 'hsocadmin.php')
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
  ajax.send(req)
}
