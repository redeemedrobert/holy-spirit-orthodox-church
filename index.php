<?php
$db = new SQLite3("hsoc.db");
if(isset($_POST['req']))
{
  if($_POST['req'] == 'getcal')
  {
    $res = new stdClass;
    $res -> type = 'getcal';
    $res -> year = $_POST['year'];
    $res -> month = $_POST['month'];
    $events = $db -> query("SELECT * FROM calendar WHERE date>='" . $_POST['date'] . "' ORDER BY date LIMIT 5");
    $i = 0;
    //'calendar'('id' INTEGER PRIMARY KEY, 'title' TEXT, 'desc' TEXT, 'year' INTEGER, 'month' INTEGER, 'day' INTEGER, 'hour' INTEGER, 'minute' INTEGER)
    while($event = $events -> fetchArray())
    {
      $res -> events[$i] = array('id' => $event['id'], 'title' => $event['title'], 'desc' => $event['desc'], 'year' => $event['year'], 'month' => $event['month'], 'day' => $event['day'], 'hour' => $event['hour'], 'minute' => $event['minute']);
      $i++;
    }
    exit(json_encode($res));
  }
  if($_POST['req'] == 'loadcal')
  {
    $res = new stdClass;
    $res -> type = 'getcal';
    $res -> year = $_POST['year'];
    $res -> month = $_POST['month'];
    $events = $db -> query("SELECT * FROM calendar WHERE year='" . $_POST['year'] . "' AND month='" . $_POST['month'] . "' ORDER BY date");
    $i = 0;
    //'calendar'('id' INTEGER PRIMARY KEY, 'title' TEXT, 'desc' TEXT, 'year' INTEGER, 'month' INTEGER, 'day' INTEGER, 'hour' INTEGER, 'minute' INTEGER)
    while($event = $events -> fetchArray())
    {
      $res -> events[$i] = array('id' => $event['id'], 'title' => $event['title'], 'desc' => $event['desc'], 'year' => $event['year'], 'month' => $event['month'], 'day' => $event['day'], 'hour' => $event['hour'], 'minute' => $event['minute']);
      $i++;
    }
    exit(json_encode($res));
  }
  if($_POST['req'] == 'getblog' || $_POST['req'] == 'getfullblog')
  {
    $res = new stdClass;
    if($_POST['req'] == 'getblog') $res -> type = 'getblog';
    else $res -> type = 'getfullblog';
    $res -> offset = $_POST['offset'];
    $q = 'SELECT * FROM blog ORDER BY date DESC';
    if(isset($_POST['lim'])) $q.= ' LIMIT ' . $_POST['lim'];
    if($_POST['offset'] > 0) $q .= ' OFFSET ' .  $_POST['offset'];
    $query = $db -> query($q);
    $count = $query -> fetchArray();
    if(!$count || $_POST['offset'] < 0)
    {
      $res -> type = 'message';
      $res -> subject = 'noblog';
      $res -> message = 'No more blog posts found.';
      exit(json_encode($res));
    }
    $query2 = $db -> query($q);
    $rowcount = 0;
    while($post = $query2 -> fetchArray())
    {
      $aquery = $db -> query("SELECT name FROM users WHERE id='" . $post['author'] . "'");
      $aname = ($aquery -> fetchArray());
      if(!$aname && $post['author'] != 0)
      {
        $authorname = 'Name Unknown';
      }
      else
      {
        if($post['author'] == 0){ $authorname = 'Holy Spirit Orthodox Church'; }
        else { $authorname = $aname[0]; }
      }
      $res -> posts[$rowcount] = array('id' => $post['id'], 'authorid' => $post['author'], 'author' => $authorname, 'title' => $post['title'], 'content' => $post['content'], 'date' => $post['date']);
      $rowcount ++;
    }
    $rows = $db -> query("SELECT count(*) FROM blog");
    $res -> rows = $rows -> fetchArray()[0];
    $res -> offset = $_POST['offset'];
    exit(json_encode($res));
  }
  if($_POST['req'] == 'blogpost')
  {
    $query = $db -> query("SELECT * FROM blog WHERE id='" . $_POST['id'] . "'");
    $post = $query -> fetchArray();
    if($post)
    {
      $aquery = $db -> query("SELECT name FROM users WHERE id='" . $post['author'] . "'");
      $aname = ($aquery -> fetchArray());
      if(!$aname && $post['author'] != 0)
      {
        $authorname = 'Name Unknown';
      }
      else
      {
        if($post['author'] == 0){ $authorname = 'Holy Spirit Orthodox Church'; }
        else { $authorname = $aname[0]; }
      }
      $res = new stdClass;
      $res -> type = 'blogpost';
      $res -> id = $post['id'];
      $res -> authorid = $post['author'];
      $res -> author = $authorname;
      $res -> title = $post['title'];
      $res -> content = $post['content'];
      $res -> date = $post['date'];
      exit(json_encode($res));
    }
    else {
      $res = new stdClass;
      $res -> type = 'message';
      $res -> subject = 'blognotfound';
      $res -> message = 'There was no blog post found with the given ID :-(';
      exit(json_encode($res));
    }
  }
  if($_POST['req'] == 'getannouncement')
  {
    $res = new stdClass;
    $res -> type = 'getannouncement';
    $query = $db -> query("SELECT value FROM settings WHERE setting='ancontent'");
    $query2 = $db -> query("SELECT value FROM settings WHERE setting='antype'");
    $ancontent = $query -> fetchArray();
    $antype = $query2 -> fetchArray();
    $res -> ancontent = $ancontent[0];
    $res -> antype = $antype[0];
    exit(json_encode($res));
  }
  if($_POST['req'] == 'getresources')
  {
    $res = new stdClass;
    $res -> type = 'getresources';
    $res -> resources = array();
    $res -> parent = $_POST['parent'];
    $query = $db -> query("SELECT * FROM resources WHERE parent='" . $_POST['parent'] . "'");
    while($result = $query -> fetchArray())
    {
      $authorname = '';
      if($result['author'] == 0){
        $authorname = 'Holy Spirit Orthodox Church';
      }else{
        $authorquery = $db -> query("SELECT name FROM users WHERE id='" . $result['id'] . "'");
        $author = $authorquery -> fetchArray();
        $authorname = '';
        if($author){
          $authorname = $author[0];
        }else{
          $authorname = 'Name Not Available';
        }
      }
      array_push($res -> resources, array('id' => $result['id'], 'author' => $result['author'], 'authorname' => $authorname, 'title' => $result['title'], 'content' => $result['content'], 'date' => $result['date'], 'category' => $result['category'], 'parent' => $result['parent']));
    }
    exit(json_encode($res));
  }
  if($_POST['req'] == 'resource')
  {
    $query = $db -> query("SELECT * FROM resources WHERE id='" . $_POST['id'] . "'");
    $post = $query -> fetchArray();
    if($post)
    {
      $aquery = $db -> query("SELECT name FROM users WHERE id='" . $post['author'] . "'");
      $aname = ($aquery -> fetchArray());
      if(!$aname && $post['author'] != 0)
      {
        $authorname = 'Name Unknown';
      }
      else
      {
        if($post['author'] == 0){ $authorname = 'Holy Spirit Orthodox Church'; }
        else { $authorname = $aname[0]; }
      }
      $res = new stdClass;
      $res -> type = 'resource';
      $res -> id = $post['id'];
      $res -> authorid = $post['author'];
      $res -> authorname = $authorname;
      $res -> title = $post['title'];
      $res -> content = $post['content'];
      $res -> date = $post['date'];
      exit(json_encode($res));
    }
    else {
      $res = new stdClass;
      $res -> type = 'message';
      $res -> subject = 'resourcenotfound';
      $res -> message = 'There was no resource found with the given ID :-(';
      exit(json_encode($res));
    }
  }
}
?>
<html>
<title>Holy Spirit Orthodox Church // Venice, FL</title>
<meta name='viewport' content='width=device-width, initial-scale=1' />
<link rel='stylesheet' type='text/css' href='theme.css' />
<script type='text/javascript' src='home.js'></script>
<body>
  <div class='header'>
    <div class='headertext'>
      <h1 class='headertitle'>Holy Spirit Orthodox Church // Venice, Fl.</h1>
    </div>
  </div>
  <div class='nav' id='nav'>
    <a onclick='loadMain()'>Welcome</a>
    <a onclick='loadCal()'>Calendar</a>
    <a onclick='loadBlog()'>News</a>
    <a onclick='loadResources()'>Resources</a>
    <button class="tithely-give-btn" data-church-id="75176">Give</button>
    <script src="https://tithe.ly/widget/v3/give.js?3"></script>
    <script>
    var tw = create_tithely_widget();
  </script>
  </div>
  <div class='content' id='content'>
    <?
    /*global $db;
    $query = ($db -> query("SELECT value FROM settings WHERE setting='ancontent'")) -> fetchArray();
    $query2 = ($db -> query("SELECT value FROM settings WHERE setting='antype'")) -> fetchArray();
    if($query2[0] == 'static')
    {
      echo <<<EOD
      <div class="staticannouncement">
      <h3>Announcement</h3>
      $query[0]
      </div>
      EOD;
    }
    else if($query2[0] == 'popup')
    {
      echo <<<EOD
      <div id="popupbg" style="display:table;">
      <a style="position: absolute; top:30; right:30; color: white; text-decoration: none;"><big>Click anywhere to close this announcement</big></a>
      <div id="popup">
      <div id="popupcont">
      <div id="popuphold">
      <h3>Announcement</h3>
      $query[0]
      </div>
      </div>
      </div>
      </div>
      EOD;
    }*/
    ?>
    <span style="display: block; font-weight: bold; margin-top: 3; text-align: center;" id="servicetimes">Sunday Liturgy: 10AM, Saturday Vespers: 5PM</span>
    <h3>Hello! We're Holy Spirit Orthodox Church and we're...</h3>
    <div class='iconflex'>
      <div class='icon' onclick="requestResource(17)">
        <img src='hello.svg' />
        <h4>Glad to meet you!</h4>
        <p>We're friendly. Everyone is welcome, whether you're an Orthodox Christian, other Christian, or not a Christian at all, we'd love to meet you.</p>
      </div>
      <div class='icon' onclick="requestResource(16)">
        <img src='cross.svg'/>
        <h4>Orthodox</h4>
        <p>We're part of the Orthodox Church in America and we live and breathe the eastern tradition of the church. Come worship with us!</p>
      </div>
      <div class='icon' onclick='requestResource(15)'>
        <img src='plant.svg' />
        <h4>Growing</h4>
        <p>From our roots as a small OCA mission in Venice, we're growing into a vibrant community of people who love Christ and each other. Come grow with us!</p>
      </div>
    </div>
    <h3>We should try to live in such a way that, if the Gospels were lost, they could be rewritten by looking at us. <br />-Metropolitan Anthony Bloom</h3>
    <div class='map'>
      <div class='mapinfo'>
        <div class='maptext'>
          <h4 class='maptextitem'>Come visit us!</h4>
          <p class='maptextitem'>Ph: 123-456-7890</p>
          <p class='maptextitem'><a href='mailto:hello@hsoc-venice.com'>hello@hsoc-venice.com</a></p>
          <p class='maptextitem'>700 Shamrock Blvd, Venice, FL 34293</p>
          <p class='maptextitem'>Sunday Divine Liturgy: 10AM</p>
        </div><div>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3552.6214006982987!2d-82.40299968527248!3d27.07369598305856!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88c35a1a9f5af001%3A0x89f6ccdeac114e12!2sHoly%20Spirit%20Orthodox%20Church!5e0!3m2!1sen!2sus!4v1570652298733!5m2!1sen!2sus" frameborder="0" allowfullscreen=""></iframe>
        </div>
      </div>
    </div>
    <h3>Upcoming Events</h3>
    <center>Click on an event title for more info - click <a herf="#nav" onclick='loadCal()'>here</a> for a full calendar.</center>
    <div id='events'>
      No upcoming events.
    </div>
    <h3>Latest News Posts</h3>
    <div id='blog'>
      No posts to show.
    </div>
  </div>
  <div class='footer'>
    <br /><small>Holy Spirit Orthodox Church - Venice, FL - Orthodox Church in America/Diocese of Dallas and the South</small><br />
  </div>
</body>
</html>
