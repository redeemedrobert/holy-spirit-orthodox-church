<?php
session_start();
$db = new SQLite3("hsoc.db");
$db -> query("CREATE TABLE IF NOT EXISTS 'users'('id' INTEGER PRIMARY KEY, 'uname' TEXT, 'name' TEXT, 'pword' TEXT, 'blogpost' INTEGER, 'calendar' INTEGER, 'announce' INTEGER, 'admin' INTEGER)");
$db -> query("CREATE TABLE IF NOT EXISTS 'settings'('setting' TEXT, 'value' TEXT)");
$db -> query("CREATE TABLE IF NOT EXISTS 'blog'('id' INTEGER PRIMARY KEY, 'author' INTEGER, 'title' TEXT, 'content' TEXT, 'date' TEXT)");
$db -> query("CREATE TABLE IF NOT EXISTS 'calendar'('id' INTEGER PRIMARY KEY, 'title' TEXT, 'desc' TEXT, 'year' INTEGER, 'month' INTEGER, 'day' INTEGER, 'hour' INTEGER, 'minute' INTEGER, 'date' INTEGER)");
$db -> query("CREATE TABLE IF NOT EXISTS 'resources'('id' INTEGER PRIMARY KEY, 'author' INTEGER, 'title' TEXT, 'date' INTEGER, 'content' TEXT, 'category' INTEGER, 'parent' INTEGER)");
//$db -> query("INSERT INTO 'settings'('setting', 'value') VALUES ('apass', '" . hash('sha256', 'comeHolySpirit777') . "')");
//$db -> query("INSERT INTO 'settings'('setting', 'value') VALUES ('ancontent', 'This is an announcement')");
//$db -> query("INSERT INTO 'settings'('setting', 'value') VALUES ('antype', 'hidden')");
$loggedin;
$failed;
$apassget = $db -> query("SELECT value FROM 'settings' WHERE setting = 'apass'");
$apass = ($apassget -> fetchArray())[0];
if(isset($_POST['uname']) && !isset($_POST['req']))
{
  if(strtolower($_POST['uname']) == 'admin' && hash('sha256', $_POST['pword']) == $apass)
  {
    $loggedin = true;
    $_SESSION['id'] = 0;
    $_SESSION['uname'] = 'admin';
    $_SESSION['name'] = 'Holy Spirit Orthodox Church';
    $_SESSION['calendar'] = true;
    $_SESSION['blogpost'] = true;
    $_SESSION['calendar'] = true;
    $_SESSION['announce'] = true;
    $_SESSION['admin'] = true;
  }
  else
  {
    $check = $db -> query("SELECT * FROM 'users' WHERE uname='" . $_POST['uname'] . "' AND pword='" . hash('sha256', $_POST['pword']) . "'");
    $count = $check -> fetchArray();
    if($count){

      $loggedin = true;
      $_SESSION['id'] = $count['id'];
      $_SESSION['uname'] = $count['uname'];
      $_SESSION['name'] = $count['name'];
      $_SESSION['calendar'] = $count['calendar'];
      $_SESSION['blogpost'] = $count['blogpost'];
      $_SESSION['announce'] = $count['announce'];
      $_SESSION['admin'] = $count['admin'];
    }
    else {
      $loggedin = false;
      $failed = true;
    }
  }
}

if(isset($_SESSION['id']) && $_SESSION['id'] != 0)
{
  $query = $db -> query("SELECT * FROM users WHERE id='" . $_SESSION['id'] . "'");
  $user = $query -> fetchArray();
  $_SESSION['uname'] = $user['uname'];
  $_SESSION['name'] = $user['name'];
  $_SESSION['calendar'] = $user['calendar'];
  $_SESSION['blogpost'] = $user['blogpost'];
  $_SESSION['announce'] = $user['announce'];
  $_SESSION['admin'] = $user['admin'];
}

if(isset($_POST['req']))
{
  if($_POST['req'] == 'init')
  {
    $res = new stdClass;
    $res -> type = 'init';
    //$res -> apass = $apass;
    $res -> id = $_SESSION['id'];
    $res -> uname = $_SESSION['uname'];
    $res -> blogpost = $_SESSION['blogpost'];
    $res -> calendar = $_SESSION['calendar'];
    $res -> announce = $_SESSION['announce'];
    $res -> admin = $_SESSION['admin'];
    exit(json_encode($res));
  }
  if($_POST['req'] == 'getblog')
  {
    $res = new stdClass;
    $res -> type = 'getblog';
    $res -> offset = $_POST['offset'];
    $q = 'SELECT * FROM blog ORDER BY date DESC LIMIT 10';
    if($_POST['offset'] > 0) $q .= ' OFFSET ' .  $_POST['offset'];
    $query = $db -> query($q);
    $count = $query -> fetchArray();
    if(!$count || $_POST['offset'] < 0)
    {
      $res -> type = 'message';
      $res -> subject = 'noblog';
      $res -> message = 'No more blog posts found.';
      exit(json_encode($res));
    }
    $query2 = $db -> query($q);
    $rowcount = 0;
    while($post = $query2 -> fetchArray())
    {
      $aquery = $db -> query("SELECT name FROM users WHERE id='" . $post['author'] . "'");
      $aname = ($aquery -> fetchArray());
      if(!$aname && $post['author'] != 0)
      {
        $authorname = 'Name Unknown';
      }
      else
      {
        if($post['author'] == 0){ $authorname = 'Holy Spirit Orthodox Church'; }
        else { $authorname = $aname[0]; }
      }
      $res -> posts[$rowcount] = array('id' => $post['id'], 'authorid' => $post['author'], 'author' => $authorname, 'title' => $post['title'], 'content' => $post['content'], 'date' => $post['date']);
      $rowcount ++;
    }
    exit(json_encode($res));
  }
  if($_POST['req'] == 'setapass')
  {
    $res = new stdClass;
    $res -> type = 'message';
    if($_SESSION['id'] == 0)
    {
      if(hash('sha256', $_POST['oldpass']) == $apass)
      {
        $db -> query('UPDATE settings SET value="' . hash('sha256', $_POST['newpass']) . '" WHERE setting="apass"');
        $res -> subject = 'apasssuccess';
        $res -> message = 'You have successfully updated the administrative password.';
        exit(json_encode($res));
      }
      else {
        $res -> subject = 'apassfail';
        $res -> message = 'Current password given does not match actual current password. Please try again.';
        exit(json_encode($res));
      }
    }
    else {
      $res -> subject = 'permissionfail';
      $res -> message = 'You do not have permission to modify this setting.';
      exit(json_encode($res));
    }
  }
  if($_POST['req'] == 'getusers')
  {
    if(!$_SESSION['admin'])
    {
      $res = new stdClass;
      $res -> type = 'message';
      $res -> subject = 'permissionfail';
      $res -> message = 'You do not have permission view users.';
      exit(json_encode($res));
    }
    $res = new stdClass;
    $res -> type = 'getusers';
    $query = $db -> query('SELECT * FROM users');
    //'users'('id' INTEGER PRIMARY KEY, 'uname' TEXT, 'name' TEXT, 'pword' TEXT, 'blogpost' INTEGER, 'calendar' INTEGER, 'admin' INTEGER)
    $rowcount = 0;
    while($post = $query -> fetchArray())
    {
      $res -> user[$rowcount] = array('id' => $post['id'], 'uname' => $post['uname'], 'name' => $post['name'], 'pword' => $post['pword'], 'blogpost' => $post['blogpost'], 'calendar' => $post['calendar'], 'announce' => $post['announce'], 'admin' => $post['admin']);
      $rowcount ++;
    }
    exit(json_encode($res));
  }
  if($_POST['req'] == 'addedituser')
  {
    if(!$_SESSION['admin'])
    {
      $res = new stdClass;
      $res -> type = 'message';
      $res -> subject = 'permissionfail';
      $res -> message = 'You do not have permission to add/edit users.';
      exit(json_encode($res));
    }
    if(!isset($_POST['id']))
    {
      $res = new stdClass;
      $res -> type = 'message';
      $unamecheck = $db -> query("SELECT * FROM users WHERE uname='" . $_POST['uname'] . "'");
      if($unamecheck -> fetchArray() !== false)
      {
        $res -> subject = 'adduserfail';
        $res -> message = 'This username is already in use. Please select different username.';
        exit(json_encode($res));
      }
      //'users'('id' INTEGER PRIMARY KEY, 'uname' TEXT, 'name' TEXT, 'pword' TEXT, 'blogpost' INTEGER, 'calendar' INTEGER, 'admin' INTEGER)
      $db -> query("INSERT INTO users ('uname', 'name', 'pword', 'blogpost', 'calendar', 'announce', 'admin') VALUES('" . $_POST['uname'] . "', '" . $_POST['name'] . "', '" .  hash('sha256', $_POST['pword']) . "', '" .  $_POST['blogpost'] . "', '" . $_POST['calendar'] . "', '" . $_POST['announce'] . "', '" . $_POST['admin'] . "')");
      $res -> subject = 'addusersuccess';
      $res -> message = $_POST['uname'] . " has been added successfully.";
      exit(json_encode($res));
    } else {
      $res = new stdClass;
      $res -> type = 'message';
      $unamequery = $db -> query("SELECT uname FROM users WHERE id='" . $_POST['id'] . "'");
      $uname = $unamequery -> fetchArray();
      if($uname[0] != $_POST['uname'])
      {
        $unamecheck = $db -> query("SELECT uname FROM users WHERE uname='" . $_POST['uname'] . "'");
        $unamelist = $unamecheck -> fetchArray();
        if($unamelist != null)
        {
          $res -> subject = 'adduserfail';
          $res -> message = 'This username is already in use. Please select different username.';
          exit(json_encode($res));
        }
      }
      //'users'('id' INTEGER PRIMARY KEY, 'uname' TEXT, 'name' TEXT, 'pword' TEXT, 'blogpost' INTEGER, 'calendar' INTEGER, 'admin' INTEGER)
      $db -> query("UPDATE users SET uname='" . $_POST['uname'] . "', name='" . $_POST['name'] . "',  blogpost='" . $_POST['blogpost'] . "', calendar='" . $_POST['calendar'] . "', announce='" . $_POST['announce'] . "', admin='" . $_POST['admin'] . "' WHERE id='" . $_POST['id'] . "'");
      $res -> subject = 'addusersuccess';
      $res -> message = $_POST['uname'] . "'s info has been updated successfully";
      if(isset($_POST['pword']))
      {
        $db -> query("UPDATE users SET pword='" . hash("sha256", $_POST['pword']) . "' WHERE id='" . $_POST['id'] . "'");
        $res -> message .= " and their password has been updated";
      }
      $res -> message .= ".";
      exit(json_encode($res));
    }
  }
  if($_POST['req'] == 'deluser')
  {
    if(!$_SESSION['admin'])
    {
      $res = new stdClass;
      $res -> type = 'message';
      $res -> subject = 'permissionfail';
      $res -> message = 'You do not have permission to add/edit users.';
      exit(json_encode($res));
    }
    $db -> query("DELETE FROM users WHERE id='" . $_POST['id'] . "'");
    $res = new stdClass;
    $res -> type = 'message';
    $res -> subject = 'delusersuccess';
    $res -> message = 'The user was deleted successfully.';
    exit(json_encode($res));
  }
  if($_POST['req'] == 'getcal')
  {
    $res = new stdClass;
    $res -> type = 'getcal';
    $res -> year = $_POST['year'];
    $res -> month = $_POST['month'];
    $events = $db -> query("SELECT * FROM calendar WHERE year='" . $_POST['year'] . "' AND month='" . $_POST['month'] . "' ORDER BY day, hour, minute");
    $i = 0;
    //'calendar'('id' INTEGER PRIMARY KEY, 'title' TEXT, 'desc' TEXT, 'year' INTEGER, 'month' INTEGER, 'day' INTEGER, 'hour' INTEGER, 'minute' INTEGER)
    while($event = $events -> fetchArray())
    {
      $res -> events[$i] = array('id' => $event['id'], 'title' => $event['title'], 'desc' => $event['desc'], 'year' => $event['year'], 'month' => $event['month'], 'day' => $event['day'], 'hour' => $event['hour'], 'minute' => $event['minute']);
      $i++;
    }
    exit(json_encode($res));
  }
  if($_POST['req'] == 'addcal')
  {
    if(!$_SESSION['calendar'])
    {
      $res = new stdClass;
      $res -> type = 'message';
      $res -> subject = 'permissionfail';
      $res -> message = 'You do not have permission to edit the calendar.';
      exit(json_encode($res));
    }
    $query = "";
    if(isset($_POST['id']))
    {
      $query = "UPDATE calendar ";
      $query .= "SET day='" . $_POST['day'] . "', ";
      $query .= "hour='" . $_POST['hour'] . "', ";
      $query .= "minute='" . $_POST['minute'] . "', ";
      $query .= "title='" . str_replace("'","''", $_POST['title']) . "', ";
      $query .= "desc='" . str_replace("'","''", $_POST['desc']) . "', ";
      $query .= "date='" . $_POST['date'] . "' ";
      $query .= "WHERE id='" . $_POST['id'] . "'";
    } else {
      $query = "INSERT INTO calendar ('title', 'desc', 'year', 'month', 'day', 'hour', 'minute', 'date') VALUES ";
      $query .= "('" . str_replace("'","''", $_POST['title']) . "', ";
      $query .= "'" . str_replace("'","''", $_POST['desc']) . "', ";
      $query .= "'" . $_POST['year'] . "', ";
      $query .= "'" . $_POST['month'] . "', ";
      $query .= "'" . $_POST['day'] . "', ";
      $query .= "'" . $_POST['hour'] . "', ";
      $query .= "'" . $_POST['minute'] . "', ";
      $query .= "'" . $_POST['date'] . "')";
    }
    $db -> query($query);
    $res = new stdClass;
    $res -> type = 'message';
    $res -> year = $_POST['year'];
    $res -> month = $_POST['month'];
    $res -> subject = 'addcalsuccess';
    $res -> message = 'Calendar event "' . $_POST['title'] . '" was added or updated successfully.';
    exit(json_encode($res));
  }
  if($_POST['req'] == 'delcal')
  {
    if(!$_SESSION['calendar'])
    {
      $res = new stdClass;
      $res -> type = 'message';
      $res -> subject = 'permissionfail';
      $res -> message = 'You do not have permission to edit the calendar.';
      exit(json_encode($res));
    }
    $db -> query("DELETE FROM calendar WHERE id='" . $_POST['id'] . "'");
    $res = new stdClass;
    $res -> type = 'message';
    $res -> subject = 'delcalsuccess';
    $res -> message = 'The calendar event was deleted successfully.';
    $res -> year = $_POST['year'];
    $res -> month = $_POST['month'];
    exit(json_encode($res));
  }
  if($_POST['req'] == 'getannouncement')
  {
    $res = new stdClass;
    $res -> type = 'getannouncement';
    $query = $db -> query("SELECT value FROM settings WHERE setting='ancontent'");
    $query2 = $db -> query("SELECT value FROM settings WHERE setting='antype'");
    $ancontent = $query -> fetchArray();
    $antype = $query2 -> fetchArray();
    $res -> ancontent = $ancontent[0];
    $res -> antype = $antype[0];
    exit(json_encode($res));
  }
  if($_POST['req'] == 'submitannouncement')
  {
    if($_SESSION['announce'])
    {
      $query = "UPDATE settings SET value='" . str_replace("'", "''", $_POST['content']) . "' WHERE setting='ancontent'";
      $db -> query($query);
      $query = "UPDATE settings SET value='" . $_POST['type'] . "' WHERE setting='antype'";
      $db -> query($query);
      $res = new stdClass;
      $res -> type = 'submitannouncement';
      $res -> message = 'The announcement has been updated successfully as the announcement type "' . $_POST['type'] . '."';
      exit(json_encode($res));
    }
    else
    {
      $res = new stdClass;
      $res -> type = 'message';
      $res -> subject = 'permissionfail';
      $res -> message = 'You do not have the required permission to make an announcement.';
      exit(json_encode($res));
    }
  }
  if($_POST['req'] == 'getimages')
  {
    $res = new stdClass;
    $res -> type = 'getimages';
    $files = scandir('uploads', SCANDIR_SORT_DESCENDING);
    $res -> images = $files;
    exit(json_encode($res));
  }
  if($_POST['req'] == 'upload')
  {
    date_default_timezone_set('America/New_York');
    $i = 0;
    $res = new stdClass;
    $count = count($_FILES['file']['name']);
    for($i; $i < $count; $i++)
    {
      $date = date('YmdHis', time());
      $loc = "uploads/$date" . "-$i-" . $_FILES['file']['name'][$i];
      move_uploaded_file($_FILES['file']['tmp_name'][$i], $loc);
      $res -> images[$i] = $loc;
    }
    $res -> type = 'message';
    $res -> subject = 'uploadsuccess';
    if($i == 1){ $res -> message = 'The file was uploaded successfully.';
    } else { $res -> message = 'The files were uploaded successfully.'; }
    exit(json_encode($res));
  }
  if($_POST['req'] == 'submitblog')
  {
    if($_SESSION['blogpost'] || $_SESSION['admin'])
    {
      if(isset($_POST['id']))
      {
        $authorquery = $db -> query("SELECT author FROM blog WHERE id='" . $_POST['id'] . "'");
        $author = $authorquery -> fetchArray();
        if($_SESSION['admin'] || $author['author'] == $_SESSION['id'])
        {
          $query = 'UPDATE blog';
          $query .= " SET title='" . str_replace("'","''", $_POST['title']);
          $query .= "', content='" . str_replace("'","''", $_POST['content']);
          $query .= "' WHERE id='" . $_POST['id'] . "'";
          $db -> query($query);
          $res = new stdClass;
          $res -> type = 'message';
          $res -> subject = 'submitblogsuccess';
          $res -> message = "The blog post \"" . $_POST['title'] . "\" has been updated successfully.";
          exit(json_encode($res));
        } else {
          $res = new stdClass;
          $res -> type = 'message';
          $res -> subject = 'permissionfail';
          $res -> message = 'You do not have permission to edit this post.';
          exit(json_encode($res));
        }
      }
      else
      {
        date_default_timezone_set('America/New_York');
        $query = "INSERT INTO blog ('title', 'content', 'author', 'date') VALUES ('";
          $query .= str_replace("'","''", $_POST['title']) . "', '";
          $query .= str_replace("'", "''", $_POST['content']) . "', '";
          $query .= $_SESSION['id'] . "', '";
          $query .= date('YmdHis') . "')";
        $db -> query($query);
        $res = new stdClass;
        $res -> type = 'message';
        $res -> subject = 'submitblogsuccess';
        $res -> message = 'New blog post "' . $_POST['title'] . '" was created successfully.';
        exit(json_encode($res));
      }
    }
    else
    {
      $res = new stdClass;
      $res -> type = 'message';
      $res -> subject = 'permissionfail';
      $res -> message = 'Blog post/edit failed, user does not have required permissions.';
      exit(json_encode($res));
    }
  }
  if($_POST['req'] == 'delblog')
  {
    $query = $db -> query("SELECT * from blog WHERE id='" . $_POST['id'] . "'");
    $post = $query -> fetchArray();
    if($_SESSION['admin'] || $_SESSION['id'] == $post['author'])
    {
      $db -> query("DELETE FROM blog WHERE id='" . $_POST['id'] . "'");
      $res = new stdClass;
      $res -> type = 'message';
      $res -> subject = 'delblogsuccess';
      $res -> message = 'The blog post was deleted successfully.';
      exit(json_encode($res));
    }
    else
    {
      $res = new stdClass;
      $res -> type = 'message';
      $res -> subject = 'permissionfail';
      $res -> message = 'You do not have the required permission to delete this post.';
      exit(json_encode($res));
    }
  }
  if($_POST['req'] == 'getresources')
  {
    $res = new stdClass;
    $res -> type = 'getresources';
    $res -> resources = array();
    $res -> parent = $_POST['parent'];
    $query = $db -> query("SELECT * FROM resources WHERE parent='" . $_POST['parent'] . "'");
    while($result = $query -> fetchArray())
    {
      $authorquery = $db -> query("SELECT name FROM users WHERE id='" . $result['id'] . "'");
      $author = $authorquery -> fetchArray();
      $authorname = '';
      if($author){ $authorname = $author[0]; }
      else{ $authorname = 'Name Not Available'; }
      array_push($res -> resources, array('id' => $result['id'], 'author' => $result['author'], 'authorname' => $authorname, 'title' => $result['title'], 'content' => $result['content'], 'category' => $result['category'], 'parent' => $result['parent']));
    }
    exit(json_encode($res));
  }
  if($_POST['req'] == 'submitresource')
  {
    if(isset($_POST['id']))
    {
      $authorquery = $db -> query("SELECT author FROM resources WHERE id='" . $_POST['id'] . "'");
      $author = $authorquery -> fetchArray();
      if($_SESSION['admin'] || $author['author'] == $_SESSION['id'])
      {
        if($_POST['category'] == '0')
        {
          $rowsquery = $db -> query("SELECT id FROM resources WHERE parent='" . $_POST['id'] . "'");
          $numrows = $rowsquery -> fetchArray();
          if($numrows)
          {
            $res = new stdClass;
            $res -> type = 'message';
            $res -> subject = 'submitresourcefail';
            $res -> message = 'You cannot change a category into a resource while the category still has child resources.';
            exit(json_encode($res));
          }
        }
        $query = 'UPDATE resources';
        $query .= " SET title='" . str_replace("'","''", $_POST['title']);
        $query .= "', content='" . str_replace("'","''", $_POST['content']);
        $query .= "', category='" . $_POST['category'];
        $query .= "' WHERE id='" . $_POST['id'] . "'";
        $db -> query($query);
        $res = new stdClass;
        $res -> type = 'message';
        $res -> subject = 'submitresourcesuccess';
        $res -> message = "The resource \"" . $_POST['title'] . "\" has been updated successfully.";
        exit(json_encode($res));
      } else {
        $res = new stdClass;
        $res -> type = 'message';
        $res -> subject = 'permissionfail';
        $res -> message = 'You do not have permission to edit this resource.';
        exit(json_encode($res));
      }
    }
    else
    {
      if($_SESSION['admin'] || $_SESSION['resources'])
      {
        date_default_timezone_set('America/New_York');
        $query = "INSERT INTO resources ('title', 'content', 'author', 'date', 'category', 'parent') VALUES ('";
          $query .= str_replace("'","''", $_POST['title']) . "', '";
          $query .= str_replace("'", "''", $_POST['content']) . "', '";
          $query .= $_SESSION['id'] . "', '";
          $query .= date('YmdHis') . "', '";
          $query .= $_POST['category'] . "', '";
          $query .= $_POST['parent'] . "')";
        $db -> query($query);
        $res = new stdClass;
        $res -> type = 'message';
        $res -> subject = 'submitresourcesuccess';
        $res -> message = 'New resource "' . $_POST['title'] . '" was created successfully.';
        exit(json_encode($res));
      }
      else
      {
        $res = new stdClass;
        $res -> type = 'message';
        $res -> subject = 'permissionfail';
        $res -> message = 'You do not have permission to create resources.';
        exit(json_encode($res));
      }
    }
  }
  if($_POST['req'] == 'delresource'){
    $query = $db -> query("SELECT id FROM resources WHERE parent='" . $_POST['id'] . "'");
    $children = $query -> fetchArray();
    if(!$children){
      $db -> query("DELETE FROM resources WHERE id='" . $_POST['id'] . "'");
      $res = new stdClass;
      $res -> type = 'message';
      $res -> subject = 'delresourcesuccess';
      $res -> message = 'The resource was deleted successfully.';
      exit(json_encode($res));
    }
    else
    {
      $res = new stdClass;
      $res -> type = 'message';
      $res -> subject = 'delresourcefail';
      $res -> message = 'Please delete all child resources before deleting this resource.';
      exit(json_encode($res));
    }
  }
}
?>
<html>
<title>HSOC Admin</title>
<?php
global $loggedin;
if($loggedin)
{
  echo("<script type='text/javascript' src='admin.js'></script>");
}
?>
<meta name='viewport' content='width=device-width, initial-scale=1' />
<link rel='stylesheet' type='text/css' href='theme-backup.css' />
<body>
  <div class='header'>
    <div class='headertext'>
      <h1 class='headertitle'>Holy Spirit Orthodox Church // Venice, Fl.</h1>
    </div>
  </div>
  <div class='nav'>
    <a href='index.php'>Home</a>
  </div>
  <div class='content'>
    <? if(isset($_SESSION['id']) && $loggedin) echo "Current user: " . $_SESSION['name'] ?>
    <? if(isset($_SESSION['id']) && ($_SESSION['admin'] || $_SESSION['calendar'])) echo '<h3>Website Administration</h3>' ?>
    <div id='container'>
      <?php
      global $apass;
      global $loggedin;
      global $failed;
      if(!$loggedin)
      {
        echo("<center>
          <form action='hsocadmin.php' method='post'>");
        if($failed) { echo("Invalid login!<br />"); }
        echo("Please log in to view this page:<br />
            <input type='text' name='uname' placeholder='username'/><input type='password' placeholder='password' name='pword' /><input type='submit' value='login' id='submit' />
          </form>
        </center>");
      }
      else {
        echo("Success!");
      }
      ?>
      <div id='resourcescont'></div>
      <div id='blogcont'></div>
    </div>
  </div>
  <div class='footer'>
</div>
</body>
</html>
